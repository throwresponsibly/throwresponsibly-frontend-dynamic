from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest
# from selenium.webdriver.common.by import By
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.support import expected_conditions as EC
# from selenium.common import exceptions

# https://selenium-python.readthedocs.io/getting-started.html
# IMPORTANT: You will need to install chrome driver from link below and
# Add the executable to PATH
# https://sites.google.com/a/chromium.org/chromedriver/downloads

class GuiTest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.url = "http://throwresponsibly.me/"

    def test_home_page(self):
        print("Validating Home Page")
        driver = self.driver
        driver.get(self.url)
        driver.find_element_by_link_text("FACILITIES").click()
        driver.find_element_by_link_text("ITEMS").click()
        driver.find_element_by_link_text("PROGRAMS").click()
        driver.find_element_by_link_text("ABOUT").click()
        driver.find_element_by_link_text("HOME").click()
        
        driver.find_element_by_class_name("root")
        driver.find_elements_by_xpath("//*[contains(text(), 'Word of the Day')]")
        driver.find_element_by_id("twitter-widget-0")

    def test_about_page(self):
        print("Validating About Page")
        driver = self.driver
        driver.get(self.url+"about")
        driver.find_element_by_class_name("statsdiv")
        driver.find_element_by_class_name("datadiv")
        driver.find_element_by_class_name("toolsdiv")
        driver.find_element_by_class_name("linksdiv")

    def test_facilities_page(self):
        print(f'Validating first card in facilities')
        driver = self.driver
        driver.get(self.url + 'facilities')
        driver.find_element_by_id('model-card').click()
        
    def test_items_page(self):
        print(f'Validating first card in items')
        driver = self.driver
        driver.get(self.url + 'items')
        driver.find_element_by_id('model-card').click()

    def test_programs_page(self):
        print(f'Validating first card in programs')
        driver = self.driver
        driver.get(self.url + 'programs')
        driver.find_element_by_id('model-card').click()

    def test_individual_facility(self):
        print(f'Validating individual facility')
        driver = self.driver
        driver.get(self.url + 'facility/Q1RQNVJfW1xGUA')
        driver.find_elements_by_xpath("//*[contains(text(), 'Type')]")
        driver.find_elements_by_xpath("//*[contains(text(), 'Dropoff')]")
        driver.find_elements_by_xpath("//*[contains(text(), 'Pickup')]")
        driver.find_elements_by_xpath("//*[contains(text(), 'Phone Number')]")
        driver.find_elements_by_xpath("//*[contains(text(), 'Website')]")
        driver.find_elements_by_xpath("//*[contains(text(), 'City')]")

    def test_individual_item(self):
        print(f'Validating individual item')
        driver = self.driver
        driver.get(self.url + 'item/445')
        driver.find_elements_by_xpath("//*[contains(text(), 'Material ID')]")
        driver.find_elements_by_xpath("//*[contains(text(), 'Definition')]")
        driver.find_elements_by_xpath("//*[contains(text(), 'Description')]")
        driver.find_elements_by_xpath("//*[contains(text(), 'Average Price')]")
        driver.find_elements_by_xpath("//*[contains(text(), 'ThrowResponsibly Score')]")

    def test_individual_program(self):
        print(f'Validating individual program')
        driver = self.driver
        driver.get(self.url + 'program/Q1RQNVJZUltDUw')
        driver.find_elements_by_xpath("//*[contains(text(), 'Type')]")
        driver.find_elements_by_xpath("//*[contains(text(), 'Dropoff')]")
        driver.find_elements_by_xpath("//*[contains(text(), 'Pickup')]")
        driver.find_elements_by_xpath("//*[contains(text(), 'Phone Number')]")
        driver.find_elements_by_xpath("//*[contains(text(), 'Website')]")
        driver.find_elements_by_xpath("//*[contains(text(), 'City')]")

    def test_pagination(self):
        print(f"Validating pagination")
        driver = self.driver
        driver.get(self.url + 'facilities')
        driver.find_element_by_id("prevbutton").click()
        driver.find_element_by_id("nextbutton").click()
    
    def test_search(self):
        print(f'Validating search')
        driver = self.driver
        driver.get(self.url + 'search')
        search = driver.find_element_by_id("search-box")
        search.send_keys("plastic walmart")
        driver.find_element_by_id("search-icon").click()
        driver.find_elements_by_xpath("//*[contains(text(), 'Facilities')]")
        driver.find_elements_by_xpath("//*[contains(text(), 'Programs')]")
        driver.find_elements_by_xpath("//*[contains(text(), 'Items')]")

    def test_facilities_search(self):
        print(f'Validating facilities search')
        driver = self.driver
        driver.get(self.url + 'facilities')
        search = driver.find_element_by_id("search-box")
        search.send_keys("Austin")
        driver.find_element_by_id("search-icon").click()
        driver.find_elements_by_xpath("//*[contains(text(), 'Randalls')]")
        driver.find_elements_by_xpath("//*[contains(text(), 'Central Market')]")
        driver.find_elements_by_xpath("//*[contains(text(), 'H-E-B')]")

    def test_programs_search(self):
        print(f'Validating programs search')
        driver = self.driver
        driver.get(self.url + 'programs')
        search = driver.find_element_by_id("search-box")
        search.send_keys("Dallas plastic")
        driver.find_element_by_id("search-icon").click()
        driver.find_elements_by_xpath("//*[contains(text(), 'Domestic Metals')]")
        driver.find_elements_by_xpath("//*[contains(text(), 'Capstone')]")

    def test_items_search(self):
        print(f'Validating items search')
        driver = self.driver
        driver.get(self.url + 'items')
        search = driver.find_element_by_id("search-box")
        search.send_keys("cardboard")
        driver.find_element_by_id("search-icon").click()

    def test_sortby_facilities(self):
        print(f'Validating facilities sort')
        driver = self.driver
        driver.get(self.url + 'facilities')
        driver.find_element_by_xpath("//*[@aria-haspopup='true']").click()
        driver.find_element_by_xpath("//*[@data-value='Name Ascending']").click()

    def test_sortby_items(self):
        print(f'Validating items sort')
        driver = self.driver
        driver.get(self.url + 'items')
        driver.find_element_by_xpath("//*[@aria-haspopup='true']").click()
        driver.find_element_by_xpath("//*[@data-value='Id Ascending']").click()
    
    def test_sortby_programs(self):
        print(f'Validating programs sort')
        driver = self.driver
        driver.get(self.url + 'programs')
        driver.find_element_by_xpath("//*[@aria-haspopup='true']").click()
        driver.find_element_by_xpath("//*[@data-value='Name Ascending']").click()

    def test_filterby_facilities(self):
        print(f'Validating facilities filter')
        driver = self.driver
        driver.get(self.url + 'facilities')
        options = driver.find_elements_by_xpath("//*[@aria-haspopup='true']")
        options[1].click()
        driver.find_element_by_xpath("//*[@data-value='Agawam']").click()

    def test_filterby_items(self):
        print(f'Validating items filter')
        driver = self.driver
        driver.get(self.url + 'items')
        options = driver.find_elements_by_xpath("//*[@aria-haspopup='true']")
        options[1].click()
        driver.find_element_by_xpath("//*[@data-value='Hard']").click()
    
    def test_filterby_programs(self):
        print(f'Validating programs filter')
        driver = self.driver
        driver.get(self.url + 'programs')
        options = driver.find_elements_by_xpath("//*[@aria-haspopup='true']")
        options[2].click()
        driver.find_element_by_xpath("//*[@data-value='True']").click()
    
    # Add more tests as needed

    def tearDown(self):
        self.driver.close()

def example():
    driver = webdriver.Chrome()
    driver.get("http://www.python.org")
    assert "Python" in driver.title
    elem = driver.find_element_by_name("q")
    elem.clear()
    elem.send_keys("pycon")
    elem.send_keys(Keys.RETURN)
    assert "No results found." not in driver.page_source
    driver.close()

if __name__ == '__main__':
    unittest.main()
