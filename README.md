Software Engineering

CS373: Fall 2018

Project: ThrowResponsibly

Name: Joseph Distler
EID: jcd3229
GitLab: jdistler
Estimated Time: 2 days
Actual Time: 1 day

Name: Jonathan Collins
EID: jrc5776
GitLab: JCollins0
Estimated Time: 2 days
Actual Time: 2 days

Name: Skylar DonLevy
EID: sed2476
GitLab: skylar-donlevy
Estimated Time: 2 day
Actual Time: 3 days

Name: Ivan IvanRadakovic
EID: ir5694
GitLab: IvanRadakovic
Estimated Time: 1 days
Actual Time: 2 days

Name: Wesley Ip
EID: whi68
GitLab: wip34
Estimated Time: 2 days
Actual Time: 1 days

https://gitlab.com/throwresponsibly/throwresponsibly-frontend-dynamic/pipelines
https://gitlab.com/throwresponsibly/throwresponsibly-backend/pipelines
http://throwresponsibly.me

SHA: e7fb6418f714b5c3e48f4f3b50ab08839ec3bbaf
