# Docker
## Build and tag the Docker image
`docker build -t frontend .`
## Spin up the container
`docker run -it -p 80:3000 --rm sample-app:latest`

Wait a minute until the app is ready, then open your browser to http://localhost:80/ and you should see the app
