import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const styles = {
  card: {
    maxWidth: 275,
    textAlign: 'left',
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
};

function DefCard(props) {
  const { classes } = props;
  const bull = <span className={classes.bullet}>•</span>;

  return (
    <Card className={classes.card}>
      <CardContent>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          Word of the Day
        </Typography>
        <Typography variant="h5" component="h2">
          re{bull}cy{bull}cling
        </Typography>
        <Typography className={classes.pos} color="textSecondary">
          verb
        </Typography>
        <Typography component="p">
          convert (waste) into reusable material.
          <br />
          {'"Recycling is fun"'}
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small" href="https://en.wikipedia.org/wiki/Recycling">Learn More</Button>
      </CardActions>
    </Card>
  );
}

DefCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DefCard);
