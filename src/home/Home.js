import React from 'react';
import {Typography, Divider, Grid} from '@material-ui/core';
import DefCard from './DefCard'
import Carousel from './Carousel'
import './Home.css'
var Hashtag = require('react-twitter-widgets').Hashtag;
var Tweet = require('react-twitter-widgets').Tweet;

const Home = () => (
  <div className="homeWrapper" id="homeWrapper">
    <Typography variant="h4" className="tagline">Learn all about recycling!</Typography>
		<Carousel />
    <Divider className="divider"/>
    <Grid container className="gridcontainer">
      <Grid item xs={6}>
        <Grid container justify="center">
          <Grid item>
            <DefCard />
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <Grid container>
          <Grid item>
            <Tweet tweetId="976201890559078400" className="tweet"/>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
    <Divider className="divider"/>
    <div className="bottomWrapper" id="bottomWrapper">
      <div className="copyright" id="copyright">
        <Typography>&copy; ThrowResponsibly 2018</Typography>
      </div>
      <div className="hashtagcont" id="hashtagcont">
        <Hashtag hashtag="ThrowResponsibly" className="hashtag" id="hashtag"/>
      </div>
    </div>
	</div>
);

export default Home;
// Somehow incorporate this
