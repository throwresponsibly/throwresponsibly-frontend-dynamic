import React , {Component} from 'react';
import MobileStepper from '@material-ui/core/MobileStepper';
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';
import './Carousel.css';

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);


const images = [
  {
    'label':'img1',
    'text' : 'Learn about what can be recycled',
    imgPath:require('../assets/images/cans.jpg'),
  },
  {
    'label':'img2',
    'text' : 'Find different recycling facilities nearby',
    imgPath:require('../assets/images/people.jpg'),
  },
  {
    'label':'img3',
    'text' : 'Find different recycling programs nearby',
    imgPath:require('../assets/images/city.jpg'),
  },
];



class Carousel extends Component {
  state = {
    activeStep: 0,
  };

  handleNext = () => {
    this.setState(prevState => ({
      activeStep: prevState.activeStep + 1,
    }));
  };

  handleBack = () => {
    this.setState(prevState => ({
      activeStep: prevState.activeStep - 1,
    }));
  };

  handleStepChange = activeStep => {
    this.setState({ activeStep });
  };


  render() {

    const { activeStep } = this.state;
    const maxSteps = images.length;

    return (
      <div className="root">
        <AutoPlaySwipeableViews
          axis={'x'}
          index={activeStep}
          onChangeIndex={this.handleStepChange}
          enableMouseEvents
          interval={5000}
        >
          {images.map((step, index) => (
            <div key={step.label}>
              {Math.abs(activeStep - index) <= 2 ? (
                <div  className="carouselContainer">
                  <div className="overlaytext">{step.text}</div>
                  <img className="img" src={step.imgPath} alt={step.label} />
                </div>
              ) : null}
            </div>
          ))}
        </AutoPlaySwipeableViews>
        <MobileStepper
          steps={maxSteps}
          activeStep={activeStep}
          variant="text"
        />
      </div>
    );
  }
}

export default Carousel;
