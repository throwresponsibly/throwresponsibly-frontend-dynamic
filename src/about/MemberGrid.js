import React, {Component} from 'react';
import {Grid, Typography} from '@material-ui/core';
import './Member.css'
import Member from './Member'

class MemberGrid extends Component{
  state = { expanded: false };

  render(){
    let memberinfo = this.props.memberinfo;
    let total_commits = memberinfo['totals']['commits'];
    let total_issues = memberinfo['totals']['issues'];
    let total_unit = memberinfo['totals']['unit'];
    memberinfo = Object.entries(memberinfo).filter((entry) => (
      entry[1]['name'] !== 't'
    ));
    let membermap = Object.values(memberinfo).map((entry) => (
      <Grid key={entry[1]['name']} item>
        <Member memberinfo={entry}/>
      </Grid>
    ));
    return  (
      <div>
        <Grid container justify="center">
          {membermap}
        </Grid>

        <div className="statsdiv">
          <Typography variant="h4" className="statslabel">Stats</Typography>
          <Typography variant="body1" className="totalcommits">{"Total Commits: " + total_commits}</Typography>
          <Typography variant="body1" className="totalissues">{"Total Issues: " + total_issues}</Typography>
          <Typography variant="body1" className="totalunit">{"Total UnitTests: " + total_unit}</Typography>
        </div>
      </div>
    );
  }
}

export default MemberGrid;
