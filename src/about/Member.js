import React, {Component} from 'react';
import {Card,CardMedia,Collapse,Typography} from '@material-ui/core';

import './Member.css'

class Member extends Component{
  state = { expanded: false };

  handleHover = () => {
    this.setState(state => ({ expanded: !state.expanded }));
  };

  render(){
    let entry = this.props.memberinfo;
    return  (
      <div>
        <Card
          className = 'card'
          onMouseEnter={this.handleHover}
          onMouseLeave={this.handleHover}
          aria-expanded={this.state.expanded}>
          <Typography variant="h6" className="name">{entry[1]['name']}</Typography>
          <CardMedia
            className='media'
            image={require('../assets/images/' + entry[1]['image'])}
            title={entry[1]['name']} alt={entry[1]['name']}
           />
           <Collapse in={this.state.expanded}>
             <Typography variant="body1" className="bio">{entry[1]['bio']}</Typography>
             <Typography variant="body1" className="job">{entry[1]['job']}</Typography>
             <Typography variant="body1" className="commits">{'Commits: ' + entry[1]['commits']}</Typography>
             <Typography variant="body1" className="issues">{'Issues: ' + entry[1]['issues']}</Typography>
             <Typography variant="body1" className="unittest">{'Unit Tests: ' + entry[1]['unit']}</Typography>
           </Collapse>
        </Card>
      </div>
    );
  }
}

export default Member;
