import React, {Component} from 'react';
import {Typography,Button,Grid} from '@material-ui/core';
import {Link} from "react-router-dom";
import MemberGrid from './MemberGrid'
import './About.css'
import ToolGrid from './ToolGrid.js'
let request = require('request');


const WESLEY_ID = ['Wesley Ip','wip34'];
const JOSEPH_ID = ['Joseph Distler', 'jdistler'];
const JONATHAN_ID = ['Jonathan Collins', 'JCollins0'];
const IVAN_ID = ['Ivan Radakovic', 'IvanRadakovic'];
const SKYLAR_ID = ['Skylar DonLevy', 'Skylar', 'skylar-donlevy'];
const TOTAL_ID = 'totals';
const COMMIT = 'commits';
const ISSUE = 'issues';
const UNIT = 'unit';
const NAME = 'name';
const IMAGE = 'image';
const BIO = 'bio';
const JOB = 'job';

const api_map = {
  "Earth911" : "http://api.earth911.com/",
  "Ebay" : "https://developer.ebay.com/my/api_test_tool",
  "Wikipedia" : "https://www.mediawiki.org/wiki/API:Main_page",
  "Google Maps": "https://cloud.google.com/maps-platform/",
  "Bing Image Search": "https://azure.microsoft.com/en-us/services/cognitive-services/bing-web-search-api/"
};



function filldata(memberinfo,unit, id,imgurl, bio, job){
  memberinfo[id] = {};
  memberinfo[id][COMMIT] = 0;
  memberinfo[id][ISSUE] = 0;
  memberinfo[id][UNIT] = unit;
  memberinfo[id][NAME] = id[0];
  memberinfo[id][IMAGE] = imgurl;
  memberinfo[id][BIO] = bio;
  memberinfo[id][JOB] = job;
}

function findKey(dict, str){
  var retKey = null;
  Object.entries(dict).forEach(([key,value]) =>{
    if(key.indexOf(str) >= 0){
      retKey = key;
      return;
    }
  });
  return retKey;
}

class About extends Component {

  constructor(){
    super();
    this.state = {
      ready:false,
      memberinfo : {},
      total_unit : 0
    };
    filldata(this.state.memberinfo,11,WESLEY_ID,'wesley.png', "I am a junior Computer Science major from Arlington TX. In my free time I love playing classical and pop music on the piano and violin.", "Frontend");
    filldata(this.state.memberinfo,5,JOSEPH_ID, 'joseph.jpg', "I am a senior Computer Science major from New York City NY. In my free time I volunteer at my local animal shelter.", "Backend-devlopment");
    filldata(this.state.memberinfo,5,JONATHAN_ID,'jonathan.jpg', "I am a junior Computer Science major from Houston, TX. I love animals and really want to learn more about recycling.", "Frontend/Backend");
    filldata(this.state.memberinfo,41,IVAN_ID,'ivan.png', "I am a junior Computer Science major from Houston TX. During football games, I lead Big Bertha in the Longhorn Band. In my free time, I play for the Texas Men’s Ultimate Frisbee team.", "Frontend/React");
    filldata(this.state.memberinfo,34,SKYLAR_ID,'skylar.jpg', "I am a junior Computer Science major from Houston TX. Video games are my passion and I love to try new things.", "Back-End/API, Design/Flask");
    filldata(this.state.memberinfo,96,TOTAL_ID);
  }

  fetch_commits(memberdict, gitlab_commits_url,page){
    let options = {
      method : 'GET',
      url: gitlab_commits_url + "?per_page=100&page="+page,
    }
    request(options, function(error, response, body){
      if( this.state.error || error || response.statusCode !== 200){
        this.setState({error: true});
        return;
      }
      var commitsJSON = JSON.parse(body);
      if(commitsJSON.length !== 0){
        for (var index in commitsJSON) {
          var commit = commitsJSON[index];
          var authorname = commit['author_name']
          var key = findKey(memberdict, authorname);
          if(key !== null){
            memberdict[key][COMMIT]++;
            memberdict[TOTAL_ID][COMMIT]++;
          }
        }
        this.fetch_commits(memberdict, gitlab_commits_url, page+1);
      }else{
        this.forceUpdate();
      }
    }.bind(this));
  }

  fetch_issues(memberdict, gitlab_issues_url,page){
    let options = {
      method : 'GET',
      url: gitlab_issues_url + "?per_page=100&page="+page,
    }
    request(options, function(error, response, body){
      if(this.state.error || error || response.statusCode !== 200){
        this.setState({error: true});
        return;
      }
      var issuesJSON = JSON.parse(body);
      if(issuesJSON.length !== 0){
        for (var index in issuesJSON) {
          var issue = issuesJSON[index];
          var assignees = issue['assignees'];
          for (var i=0; i < assignees.length; i++) {
            var person = assignees[i];
            var authorname = person['name'];
            var key = findKey(memberdict, authorname);
            if(key !== null){
              memberdict[key][ISSUE]++;
              memberdict[TOTAL_ID][ISSUE]++;
            }
          }
        }
        this.fetch_issues(memberdict, gitlab_issues_url, page+1);
      }else{
        this.forceUpdate();
      }
    }.bind(this));
  }

  componentWillMount(){
    // Called the first time the component is loaded right before
    // it is added to the page
    this.setState({error: false});
    var memberdict = this.state.memberinfo;
    // gitlab api urls
    var gitlab_frontend_static_commits_url = "https://gitlab.com/api/v4/projects/8623060/repository/commits";
    var gitlab_frontend_dynamic_commits_url = "https://gitlab.com/api/v4/projects/8954102/repository/commits";
    var gitlab_backend_commits_url = "https://gitlab.com/api/v4/projects/8939002/repository/commits";

    var gitlab_frontend_static_issues_url = "https://gitlab.com/api/v4/projects/8623060/issues";
    var gitlab_frontend_dynamic_issues_url = "https://gitlab.com/api/v4/projects/8954102/issues";
    var gitlab_backend_issues_url = "https://gitlab.com/api/v4/projects/8939002/issues";

    // asynchronously loaded data from GitLab-API
    this.fetch_issues(memberdict, gitlab_frontend_static_issues_url,1);
    this.fetch_issues(memberdict, gitlab_backend_issues_url,1);
    this.fetch_issues(memberdict, gitlab_frontend_dynamic_issues_url,1);
    this.fetch_commits(memberdict, gitlab_frontend_static_commits_url,1);
    this.fetch_commits(memberdict, gitlab_frontend_dynamic_commits_url,1);
    this.fetch_commits(memberdict, gitlab_backend_commits_url,1);
    this.setState({memberinfo : memberdict});
  }


  render(){
    let memberGrid = <MemberGrid memberinfo={this.state.memberinfo}/>
    let dataitems = Object.entries(api_map).map((entry) => (
      <div key={entry[0]}>
        <Button id={entry[0]}><a className="datalink" href={entry[1]}>{entry[0]}</a></Button>
      </div>
    ));

    return (
      <div className="aboutdiv">
        <div className="wrapper">
          <Typography variant="h2">About</Typography>
          <div className="aboutText">

           <p className="purpose">
              ThrowResponsibly collects information on what types of humans
              byproducts are recyclable and on facilities where these items can be
              recycled properly. It also gathers information on recycling programs.
              This site is intended for anyone that is environmentally conscious but
              does not have information on what can be recycled and where.
            </p>

            <p className="goal">
              By combining this information, ThrowResponsibly guides its users
              through the whole journey from discovering what items they can recycle
              to where they can recycle them. Furthermore, it informs users what
              recycling programs they can join to best fit their needs.
            </p>
          </div>
          <Typography variant="h4">Team</Typography>
          {memberGrid}

          <div className="datadiv">
            <Typography variant="h4">Data</Typography>
            <div className="dataitems">
              <Grid container justify="center">
                {dataitems}
              </Grid>
            </div>
            <Typography variant="body1">
              Data was scraped in Python using requests module to
              send HTTP requests to RESTful API's with custom payloads.
            </Typography>
          </div>
          <div className="toolsdiv">
            <Typography variant="h4">Tools</Typography>
            <ToolGrid />
          </div>
          <div className="visualdiv">
            <Typography variant="h4">Visualizations</Typography>
            <Button variant="outlined" className="visuallink" component={Link} to="/visual/item">Items Visualization</Button>
            <Button variant="outlined" className="visuallink" component={Link} to="/visual/program">Programs Visualization</Button>
            <Button variant="outlined" className="visuallink" component={Link} to="/visual/facility">Facilities Visualization</Button>
            <Button variant="outlined" className="visuallink" component={Link} to="/visual/df_shelter">DF shelter Visualization</Button>
            <Button variant="outlined" className="visuallink" component={Link} to="/visual/df_org">DF org. Visualization</Button>
            <Button variant="outlined" className="visuallink" component={Link} to="/visual/df_disaster">DF disaster Visualization</Button>
          </div>
          <div className="linksdiv">

            <Button variant="outlined" className="gitlablink" href="https://gitlab.com/throwresponsibly">Gitlab Repository</Button>
            <Button variant="outlined" className="postmanlink" href="https://documenter.getpostman.com/view/5500859/RWgm2gF8#089283ef-c7c4-4be9-bfad-88ce4d000408">Postman API</Button>
          </div>

        </div>
      </div>
    );

  }
}

export default About;
