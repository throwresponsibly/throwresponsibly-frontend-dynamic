import React, {Component} from 'react';
import {Typography,Grid, Card,CardMedia, Collapse} from '@material-ui/core';
import './Tool.css'

class Tool extends Component{
  state = { expanded: false };

  handleHover = () => {
    this.setState(state => ({ expanded: !state.expanded }));
  };

  render(){

    return(
      <Grid item className="tooldiv">
        <div>
          <Card
            className="toolcard"
            onMouseEnter={this.handleHover}
            onMouseLeave={this.handleHover}
            aria-expanded={this.state.expanded}
          >
            <div className="logodiv">
              <CardMedia className="toolmedia" image={this.props.logo_url}></CardMedia>
            </div>
            <Typography variant="h6">{this.props.name}</Typography>
            <Collapse in={this.state.expanded}>
            <div className="textdiv">
              <Typography variant="body1">{this.props.desc}</Typography>
            </div>
            </Collapse>
          </Card>
        </div>
      </Grid>
    );
  }
}

export default Tool;
