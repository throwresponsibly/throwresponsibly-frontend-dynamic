import React, {Component} from 'react';
import {Grid} from '@material-ui/core';

import Tool from './Tool'

const tools_map= {
  "GCP":
    {
      "desc":"cloud hosting of the website",
      "logo_url": "https://logo.clearbit.com/google.com"
    },
  "Slack":
  {
    "desc":"communication and collaboration",
    "logo_url": "https://logo.clearbit.com/slack.com"
  },
  "Docker":
  {
    "desc":"ensuring everyone on the team has the same dev environment",
    "logo_url": "https://logo.clearbit.com/docker.com"
  },
  "Postman":
  {
    "desc":"develop and design our APIs",
    "logo_url": "https://logo.clearbit.com/getpostman.com"
  },
  "ReactJS":
  {
    "desc":"frontend framework",
    "logo_url": "https://logo.clearbit.com/reactjstutorial.net"
  },
  "Material UI":
  {
    "desc":"frontend theme",
    "logo_url": "https://logo.clearbit.com/material-ui.com"
  },
  "Gitlab":
  {
    "desc":"manage our repository and CI/CD",
    "logo_url": "https://logo.clearbit.com/gitlab.com"
  },
  "PostgreSQL":
  {
    "desc":"backend database",
    "logo_url": "https://logo.clearbit.com/postgresql.org"
  },
  "JestJS":
  {
    "desc":"frontend unit testing",
    "logo_url": "https://logo.clearbit.com/jestjs.io"
  },
  "Selenium":
  {
    "desc":"frontend acceptance testing",
    "logo_url": "https://logo.clearbit.com/seleniumhq.org"
  },
  "Python":
  {
    "desc":"backend",
    "logo_url": "https://logo.clearbit.com/python.org"
  },
  "SQL Alchemy":
  {
    "desc":"backend database connection framework",
    "logo_url": "https://logo.clearbit.com/sqlalchemy.org"
  },
}

class ToolGrid extends Component{

  render(){
    let toolitems = Object.entries(tools_map).map((entry)=>(
        <Tool key={entry[0]} name={entry[0]} desc={entry[1]['desc']} logo_url={entry[1]['logo_url']}/>
    ));
    return  (
      <Grid container justify="center">
        {toolitems}
      </Grid>
    );
  }
}

export default ToolGrid;
