import React, { Component } from 'react';
import SearchBar from './SearchBar';
import Model from '../model/Model';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import './Search.css'
var request = require('request');

function format_string(str){
  return str.replace(/ /g,"%20")
}

class Search extends Component {

  constructor(props){
    super(props);
    this.state = {
      fac_items:[],
      item_items:[],
      prog_items:[],
    }
    this.fsdata = {
      search: "",
      searched: false,
      fac_page:1,
      fac_maxpage:1,
      prog_page:1,
      prog_maxpage:1,
      item_page:1,
      item_maxpage: 1,
    }
  }
  search = terms =>{
    this.fsdata.search = terms;
    this.get_facilities_search_results(1);
    this.get_programs_search_results(1);
    this.get_items_search_results(1);
    this.fsdata.searched = true;
  };

  get_page_url(index,model){
    var searchVal = this.fsdata.search ? this.fsdata.search : "*";
    var params = "&search=" + format_string(searchVal);
    return "http://api.throwresponsibly.me/"+model+"?size=4&page="+index+params;
  }

  get_facilities_search_results(index){
		let options = {
      method : 'GET',
      url: this.get_page_url(index,"facilities"),
    }
    request(options, function(error, response, body){
      if( this.state.error || error || response.statusCode !== 200){
        this.setState({error: true});
        return;
      }
			var json = JSON.parse(body)
			var result = json['result'];
      var max_page = json['max_page'];
			var sepItems = [];
			for(var i = 0; i < result.length; i++){
				let entry = result[i];
				var item =
				{
						"name":entry['name'],
						"image":entry['image_url'],
						"line1":"Type: "+entry['facility_type'],
						"line2":"Dropoff: "+entry['dropoff'],
						"line3":"Pickup: "+entry['pickup'],
						"line4":"Phone: "+entry['phone'],
						"line5":"City: "+entry['city'],
						"id":entry['id'],
				}
				sepItems.push(item)
			}
      this.fsdata.fac_maxpage=max_page;
			this.setState({fac_items:sepItems});
    }.bind(this));
	}

  get_programs_search_results(index){
		let options = {
      method : 'GET',
      url: this.get_page_url(index,"programs"),
    }
    request(options, function(error, response, body){
      if( this.state.error || error || response.statusCode !== 200){
        this.setState({error: true});
        return;
      }
			var json = JSON.parse(body)
			var result = json['result'];
      var max_page = json['max_page'];
			var sepItems = [];
			for(var i = 0; i < result.length; i++){
				let entry = result[i];
				var item =
				{
						"name":entry['name'],
						"image":entry['image_url'],
						"line1":"Type: "+entry['program_type'],
						"line2":"Dropoff: "+entry['dropoff'],
						"line3":"Pickup: "+entry['pickup'],
						"line4":"Phone: "+entry['phone'],
						"line5":"City: "+entry['city'],
						"id":entry['id'],
				}
				sepItems.push(item)
			}
      this.fsdata.prog_maxpage=max_page;
			this.setState({prog_items:sepItems})
      this.forceUpdate();
    }.bind(this));
	}

  get_items_search_results(index){
		let options = {
			method : 'GET',
			url: this.get_page_url(index,"items"),
		}
		request(options, function(error, response, body){
			if( this.state.error || error || response.statusCode !== 200){
				this.setState({error: true});
				return;
			}
			var json = JSON.parse(body)
			var result = json['result'];
      var max_page = json['max_page'];
			var sepItems = [];
			for(var i = 0; i < result.length; i++){
				let entry = result[i];
				var price = entry['avg_price']
				var scoreText = entry['score'];

				var item =
				{
						"name":entry['name'],
						"image":entry['img_url'],
						"line1":"Score: " + scoreText,
						"line2":"Price: $"+ price,
						"line3":"Family Id: " + entry['family_ids'][0],
						"line4":"Id: "+entry['id'],
						"line5":"",
						"id":entry['id'],
				}
				sepItems.push(item)
			}
      this.fsdata.item_maxpage=max_page;
			this.setState({item_items:sepItems})
      this.forceUpdate();
		}.bind(this));
	}

  handleNextClick(model){
    if(!this.fsdata.searched)
      return;
    var curr_page = 1;
    var end_index = 1;
    switch(model){
      case "fac":
      curr_page = this.fsdata.fac_page;
      end_index = this.fsdata.fac_maxpage;
      break;
      case "prog":
      curr_page = this.fsdata.prog_page;
      end_index = this.fsdata.prog_maxpage;
      break;
      case "item":
      curr_page = this.fsdata.item_page;
      end_index = this.fsdata.item_maxpage;
      break;
    }
		var index = curr_page+1 > end_index ? curr_page : curr_page +1;
		if(index === curr_page){
			return;
		}

    switch(model){
      case "fac":
      this.get_facilities_search_results(index);
      this.fsdata.fac_page++;
      break;
      case "prog":
      this.get_programs_search_results(index);
      this.fsdata.prog_page++;
      break;
      case "item":
      this.get_items_search_results(index);
      this.fsdata.item_page++;
      break;
    }
	}
	handlePrevClick(model){
    if(!this.fsdata.searched)
      return;
    var curr_page = 1;
    switch(model){
      case "fac":
      curr_page = this.fsdata.fac_page;
      break;
      case "prog":
      curr_page = this.fsdata.prog_page;
      break;
      case "item":
      curr_page = this.fsdata.item_page;
      break;
    }
    var index = curr_page-1 < 1 ? curr_page : curr_page-1;
		if(index === curr_page){
			return;
		}

    switch(model){
      case "fac":
      this.get_facilities_search_results(index);
      this.fsdata.fac_page--;
      break;
      case "prog":
      this.get_programs_search_results(index);
      this.fsdata.prog_page--;
      break;
      case "item":
      this.get_items_search_results(index);
      this.fsdata.item_page--;
      break;
    }

	}

  render() {
    var fac_result = this.state.fac_items.length > 0 ?
    <div>
      <div className="button" onClick={this.handlePrevClick.bind(this,"fac")}><KeyboardArrowLeft/></div>
        <div className="result-div" id="facilities-results">
          <Model title="Facilities" modelItems={this.state.fac_items}/>
        </div>
      <div className="button" onClick={this.handleNextClick.bind(this,"fac")}><KeyboardArrowRight/></div>
    </div>
    : <div></div>

    var prog_result = this.state.prog_items.length > 0 ?
    <div>
      <div className="button" onClick={this.handlePrevClick.bind(this,"prog")}><KeyboardArrowLeft/></div>
        <div className="result-div" id="programs-results">
          <Model title="Programs" modelItems={this.state.prog_items}/>
        </div>
      <div className="button" onClick={this.handleNextClick.bind(this,"prog")}><KeyboardArrowRight/></div>
    </div>
    : <div></div>

    var item_result = this.state.item_items.length > 0 ?
    <div>
      <div className="button" onClick={this.handlePrevClick.bind(this,"item")}><KeyboardArrowLeft/></div>
        <div className="result-div" id="items-results">
          <Model title="Items" modelItems={this.state.item_items}/>
        </div>
      <div className="button" onClick={this.handleNextClick.bind(this,"item")}><KeyboardArrowRight/></div>
    </div>
    : <div></div>
		return (
		  <div className="search-root" id="search-root">
        <SearchBar onRequestSearch={this.search} id="searchbar-comp"/>
        <div className="results-div" id="results-div">
            {fac_result}
            {prog_result}
            {item_result}
            {this.state.fac_items.length == 0 && this.state.prog_items.length == 0 &&
              this.state.item_items.length == 0 ?
              (<p>No results</p>)
              : (<div></div>)
            }

        </div>
		  </div>
		);
	}
}

export default Search;
