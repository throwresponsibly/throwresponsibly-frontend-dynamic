import React from 'react';
import Enzyme from 'enzyme'
import {shallow} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Search from './Search';

Enzyme.configure({ adapter: new Adapter() });

it('renders without crashing', () => {
  shallow(<Search />);
});

it('defaults to empty text since state is set to not ready', () => {
  const search =  shallow(<Search />);
  expect(search.text()).toEqual("<SearchBar /> ");
});

it('fac items', () => {
  const search =  shallow(<Search/>);
  expect(search.state().fac_items).toEqual([]);
});

it('item items', () => {
  const search =  shallow(<Search/>);
  expect(search.state().item_items).toEqual([]);
});

it('prog items', () => {
  const search =  shallow(<Search/>);
  expect(search.state().prog_items).toEqual([]);
});
