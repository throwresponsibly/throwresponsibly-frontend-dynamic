import React, { Component } from 'react';
import {TextField,Button} from '@material-ui/core';
import Search from '@material-ui/icons/Search';
import './Search.css'

class SearchBar extends Component {

  state = {
    value: "",
  }
  onChange = event =>{
    this.setState({value:event.target.value});
  };

  handleSearch = terms =>{
    this.props.onRequestSearch(this.state.value);
  };

  render() {
		return (
		  <div className="search-root" id="search-root">
        <div className="search-field" id="search-field">
          <TextField
          id="search-box"
          label="Search"
          style={{ margin: '0 auto',
                    height: 'auto',}}
          placeholder="Search terms"
          onChange={this.onChange}
          margin="normal"
          onKeyPress={(event) => {
            if(event.key === 'Enter'){
              this.handleSearch();
            }
          }}
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          />
        </div>
        <div className="search-icon-div">
          <Button id="search-button" onClick={this.handleSearch}>
            <Search id="search-icon"/>
          </Button>
        </div>
		  </div>
		);
	}
}

export default SearchBar;
