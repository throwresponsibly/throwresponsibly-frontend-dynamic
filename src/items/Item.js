import React, { Component } from 'react';
import {Typography, Grid, Card, CardMedia} from '@material-ui/core'
import { Link} from "react-router-dom";
import './Item.css'
var request = require('request');


class Item extends Component{


  constructor(props){
    super(props);
    this.state={
      ready:false,
      id:props.match.params.id,
    }
  }

  componentWillMount(){
    let options = {
			method : 'GET',
			url: "http://api.throwresponsibly.me/items/"+this.state.id,
		}
		request(options, function(error, response, body){
			if( this.state.error || error || response.statusCode !== 200){
				this.setState({error: true});
				return;
			}
			var json = JSON.parse(body)
      this.setState({
        ready:true,
        name:json['name'],
        image_url:json['img_url'],
        long_desc:json['long_desc'],
        avg_price: json['avg_price'],
        score: json['score'],
        definition:json['definition'],
        facilities: json['facilities'].slice(0,3),
        programs:json['programs'].slice(0,3),
        listings: json['listings'],
      });
      this.forceUpdate();
		}.bind(this));
  }

  redirect(url){
    window.location=url;
  }

  render(){
    let facilities = <div>No Facilities Available</div>;
    let programs = <div>No Programs Available</div>;
    let listings = <div> No listings Available</div>;
    if (this.state.ready){
      if(this.state.facilities.length > 0){
        facilities = Object.entries(this.state.facilities).map((facility) =>(
          <Grid item key={facility[1]['id']}>
            <Card component={Link} to={"/facility/"+facility[1]['id']} className="model-card">
              <CardMedia className="model-card-media" image={facility[1]['image_url']} />
              <Typography>{facility[1]['name']}</Typography>
            </Card>
          </Grid>
        ));
      }
      if(this.state.programs.length > 0){
        programs = Object.entries(this.state.programs).map((program) =>(
          <Grid item key={program[1]['id']}>
            <Card component={Link} to={"/program/"+program[1]['id']} className="model-card">
              <CardMedia className="model-card-media" image={program[1]['image_url']} />
              <Typography>{program[1]['name']}</Typography>
            </Card>
          </Grid>
        ));
      }
      if(this.state.listings['image_urls'].length > 0){
        listings = Object.entries(this.state.listings['image_urls']).map((entry)=>(
          <Grid item key={this.state.listings['titles'][entry[0]]}>
            <Card className="model-card" onClick={this.redirect.bind(this,this.state.listings['item_web_url'][entry[0]])}>
              <CardMedia className="model-card-media" image={entry[1]} />
              <Typography>{this.state.listings['titles'][entry[0]]}</Typography>
            </Card>
          </Grid>
        ));
      }
    }
    return(
      <div className="itemWrapper" id="itemWrapper">
        <div className="titlediv" id="titlediv">
          <Typography variant="h2" id="title">{this.state.name}</Typography>
        </div>
        <div className="imagewrapperdiv" id="imagewrapperdiv">
          <div className="imagediv" id="imagediv">
            <img src={this.state.image_url} className="itemImage" alt="Img CORS protected"></img>
          </div>
        </div>
        <div className="propsdiv" id="propsdiv">
          <div className="propstextdiv" id="propstextdiv">
            <Typography variant="h4">Stats</Typography>
            <Typography variant="body1"><b>Material ID:</b> {this.state.id}</Typography>
            <Typography variant="body1"><b>Definition:</b> {this.state.definition}</Typography>
            <Typography variant="body1"><b>Description:</b> {this.state.long_desc}</Typography>
            <Typography variant="body1"><b>Average Price:</b> ${this.state.avg_price}</Typography>
            <Typography variant="body1"><b>ThrowResponsibly Score:</b> {this.state.score}</Typography>
          </div>
        </div>
        <div className="facilitiesdiv" id="facilitiesdiv">
          <Typography variant="h5">Facilities where you can donate this item:</Typography>
          <Grid container justify="center">
            {facilities}
          </Grid>
        </div>
        <div className="programsdiv" id="programsdiv">
          <Typography variant="h5">Programs you can use to donate this item:</Typography>
          <Grid container justify="center">
            {programs}
          </Grid>
        </div>
        <div className="ebaydiv" id="ebaydiv">
          <Typography variant="h5">Ebay listings for this item:</Typography>
          <Grid container justify="center">
            {listings}
          </Grid>
        </div>
      </div>
    );
  }
}

export default Item;
