import React from 'react';
import Enzyme from 'enzyme'
import {shallow} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Item from './Item';

Enzyme.configure({ adapter: new Adapter() });

it('renders without crashing', () => {
  shallow(<Item match={{params: {id: 1}}}/>);
});

it('defaults item', () => {
  const item =  shallow(<Item match={{params: {id: 1}}}/>);
  expect(item.text()).toEqual("<WithStyles(Typography) /><WithStyles(Typography) /><WithStyles(Typography) /><WithStyles(Typography) /><WithStyles(Typography) /><WithStyles(Typography) /><WithStyles(Typography) /><WithStyles(Typography) /><WithStyles(Grid) /><WithStyles(Typography) /><WithStyles(Grid) /><WithStyles(Typography) /><WithStyles(Grid) />");
});

it('item ready', () => {
  const item =  shallow(<Item match={{params: {id: 1}}}/>);
  expect(item.state().ready).toEqual(false);
});

it('item id', () => {
  const item =  shallow(<Item match={{params: {id: 1}}}/>);
  expect(item.state().id).toEqual(1);
});
