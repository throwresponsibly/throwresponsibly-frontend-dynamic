import React from 'react';
import Enzyme from 'enzyme'
import {shallow} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Items from './Items';

Enzyme.configure({ adapter: new Adapter() });

it('renders without crashing', () => {
  shallow(<Items />);
});

it('defaults to empty text since state is set to not ready', () => {
  const items =  shallow(<Items />);
  expect(items.text()).toEqual('');
});

it('defaults to page 1', () => {
  const items =  shallow(<Items/>);
  expect(items.state().page).toEqual(1);
});

it('num of sort terms', () => {
  const items =  shallow(<Items/>);
  expect(items.state().sortterms.length).toEqual(6);
});

it('num of score terms', () => {
  const items =  shallow(<Items/>);
  expect(items.state().filtscoreterms.length).toEqual(5);
});

it('num of family terms', () => {
  const items =  shallow(<Items/>);
  expect(items.state().filtfamilyterms.length).toEqual(28);
});
