import React, { Component } from 'react';
import Model from '../model/Model';
import {Button, MenuItem} from '@material-ui/core';
import SimpleSelect from '../model/SimpleSelect'
import SearchBar from '../search/SearchBar';
import './Items.css'
var request = require('request');

var sortbyterms = [
  "Name Ascending",
  "Name Descending",
	"Price Ascending",
	"Price Descending",
	"Id Ascending",
	"Id Descending",
];

var filterbyfamilyterms =["1 Automobile","2 Batteries","3 Construction",
"4 Electronics","5 Glass","6 Metal","7 Paint","8 Paper","9 Plastic","78 Paper",
"79 Glass","80 Electronics","81 Metals","82 Batteries","83 Reuse","84 Other",
"85 Hazardous","86 Construction","93 Organics","94 Rubbermaid","95 Hazardous",
"96 Organics","97 Reuse","98 Household","101 Holiday","102 Garage",
"106 Unsponsored","108 All"];

var filterbyscoreterms =["Really Hard","Hard","Medium","Easy","Really Easy"];

function format_string(str){
  return str.replace(/ /g,"%20")
}

class Items extends Component {
	constructor(){
		super();


		this.handleNextClick = this.handleNextClick.bind(this);
		this.handlePrevClick = this.handlePrevClick.bind(this);

    var sortterms = sortbyterms.map((term) =>
			<MenuItem key={term} value={term}>{term}</MenuItem>
		);

		var filtscoreterms = filterbyscoreterms.map((term) =>
			<MenuItem key={term} value={term}>{term}</MenuItem>
		);
		var filtfamilyterms = filterbyfamilyterms.map((term) =>
			<MenuItem key={term} value={term}>{term}</MenuItem>
		);

    this.state = {
			page:1,
      maxpage: 1,
      sortterms:sortterms,
      filtscoreterms: filtscoreterms,
      filtfamilyterms: filtfamilyterms,
		}

    this.fsdata = {
      sort_on:"None",
      filter_score:"None",
      filter_fam:"None",
      search: "",
      sort_order:"ASC",
    }
	}

	get_page_url(index){
    var params = "";
    if(this.fsdata.sort_on !== "None"){
      params += "&sort="+this.fsdata.sort_on;
      params += "&sort_order="+this.fsdata.sort_order;
    }
    if(this.fsdata.filter_score !== "None")
      params += "&score_filter="+this.fsdata.filter_score;
    if(this.fsdata.filter_fam !== "None")
      params += "&family_filter="+this.fsdata.filter_fam;
      params += "&search=" + format_string(this.fsdata.search);
		return "http://api.throwresponsibly.me/items?size=12&page="+index+params;
	}

	get_page(index){
		let options = {
			method : 'GET',
			url: this.get_page_url(index),
		}
		request(options, function(error, response, body){
			if( this.state.error || error || response.statusCode !== 200){
				this.setState({error: true});
				return;
			}
			var json = JSON.parse(body)
			var result = json['result'];
      var max_page = json['max_page'];
			var sepItems = [];
			for(var i = 0; i < result.length; i++){
				let entry = result[i];
				var price = entry['avg_price']
				var item =
				{
						"name":entry['name'],
						"image":entry['img_url'],
						"line1":"Score: " + entry['score'],
						"line2":"Price: $"+ price,
						"line3":"Family Id: " + entry['family_ids'][0],
						"line4":"Id: "+entry['id'],
						"line5":"",
						"id":entry['id'],
				}
				sepItems.push(item)
			}
			this.setState({items:sepItems,nextLink: "http://api.throwresponsibly.me"+result['next'],maxpage:max_page ,ready:true})
		}.bind(this));
	}

	componentWillMount(){
		this.get_page(1);
	}


	handleNextClick(){
		var index = this.state.page+1 > this.state.maxpage ? this.state.page : this.state.page +1;
		if(index === this.state.page){
			return;
		}
		this.setState({page: index})
		this.get_page(index);
	}
	handlePrevClick(){
		var index = this.state.page-1 < 1 ? this.state.page : this.state.page-1;
		if(index === this.state.page){
			return;
		}
		this.setState({page: index})
		this.get_page(index);
	}

	apply = (label, value) =>{
    //set variables for api
    if(label === "Score"){
      this.fsdata.filter_score=value;
    }else if (label === "Family") {
      this.fsdata.filter_fam=value.split(" ")[0];
    }else if(label === "Sort By"){
      this.fsdata.sort_on=value.toLowerCase().split(" ")[0];
      var order = value.toLowerCase().split(" ")[1];
      if(order === "descending")
        this.fsdata.sort_order= "DESC"
      else {
        this.fsdata.sort_order= "ASC"
      }
    }
    this.get_page(1);
  };

  search = terms =>{
    //set variables for api
    this.fsdata.search = terms
    this.get_page(1);
  };

	render() {

		if(this.state.ready){
			return (
				<div className="model-root" id="model-root">
					<div className="selector-div" id="selector-div">
						<div className="selector" id="sortselector">
							<SimpleSelect label="Sort By" items={this.state.sortterms} callback={this.apply}/>
						</div>
						<div className="selector" id="filtersselector">
							<SimpleSelect label="Score" items={this.state.filtscoreterms} callback={this.apply}/>
						</div>
						<div className="selector" id="filterfselector">
							<SimpleSelect label="Family" items={this.state.filtfamilyterms} callback={this.apply}/>
						</div>
            <div className="selector">
              <SearchBar onRequestSearch={this.search} id="searchbar-comp"/>
            </div>
					</div>
				<Model title="Items" modelItems={this.state.items} nextLink={this.state.nextLink}/>
				<div className="model-footer" id="model-footer">
					<Button variant="contained" onClick={this.handlePrevClick} id="prevbutton">Prev</Button>
					<span className="model-pagination" id="model-pagination">Page {this.state.page} of {this.state.maxpage}</span>
					<Button variant="contained" onClick={this.handleNextClick} id="nextbutton">Next</Button>
				</div>
				</div>
			);
		}else{
			return("");
		}
	}
}

export default Items;
