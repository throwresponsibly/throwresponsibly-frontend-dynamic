import React from 'react';
import Enzyme from 'enzyme'
import {shallow} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Programs from './Programs';

Enzyme.configure({ adapter: new Adapter() });

it('renders without crashing', () => {
  shallow(<Programs />);
});

it('defaults to empty text since state is set to not ready', () => {
  const programs =  shallow(<Programs />);
  expect(programs.text()).toEqual('');
});

it('defaults to page 1', () => {
  const programs =  shallow(<Programs/>);
  expect(programs.state().page).toEqual(1);
});

it('num of sort terms', () => {
  const programs =  shallow(<Programs/>);
  expect(programs.state().sortterms.length).toEqual(4);
});

it('num of city terms', () => {
  const programs =  shallow(<Programs/>);
  expect(programs.state().filtcityterms.length).toEqual(96);
});

it('num of drop off terms', () => {
  const programs =  shallow(<Programs/>);
  expect(programs.state().filtdropoffterms.length).toEqual(2);
});

it('num of pick up terms', () => {
  const programs =  shallow(<Programs/>);
  expect(programs.state().filtpickupterms.length).toEqual(2);
});
