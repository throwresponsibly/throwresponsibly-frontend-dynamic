import React, { Component } from 'react';
import Model from '../model/Model';
import {Button, MenuItem} from '@material-ui/core';
import SimpleSelect from '../model/SimpleSelect';
import SearchBar from '../search/SearchBar';
import './Programs.css'

var request = require('request')

var filtercityterms = ["Alton","Annapolis","Atlanta","Austin","Avondale","Boulder",
"Brooklyn","Camarillo","Carrollton","Chatsworth","Chicago","Cleveland","Coconut Creek",
"Cranford","Dallas","Deer Park","Denver","Downey","Elkhart","Erie","Evansville",
"Fairfield","Federal Heights","FORT WAYNE","Gibsonia","Gilbert","Glendale","Hammond",
"Hilliard","Hoffman Estates","Holyoke","Hull","Indianapolis","Janesville","Jefferson",
"Lancaster","Lincolnshire","Lisle","Little Rock","Liverpool","Long Beach","Los Angeles",
"Los Gatos","Louisville","Loveland","Mechanicville","Medford","Memphis","Methuen",
"Milwaukee","Milwaukie","Minneapolis","Moline","Nashville","New Albany","New York",
"Norcross","North Kansas City","North Ridgeville","Ojai","Old Tappan","Olmsted Falls",
"Palm Desert","Philadelphia","Phoenix","Pittsburgh","Portland","Roanoke","Round Rock",
"Salem","San Diego","San Rafael","Santa Fe Springs","Sapulpa","Scottsdale","Seattle",
"Secaucus","Sharon","Sherman Oaks","Signal Hill","Skokie","Solon","sparks","Sparks",
"St.Charles","St Louis","Stockbridge","Tampa","Tigard","Toledo","VANCOUVER",
"Vernon","Victor","Warrensville Heights","Windsor","Wooster"];

var sortbyterms = [
  "Name Ascending",
  "Name Descending",
  "City Ascending",
  "City Descending",
];
var filterdropoffterms = [
  "True",
	"False"
];
var filterpickupterms = [
	"True",
	"False"
];

function format_string(str){
  return str.replace(/ /g,"%20")
}

class Programs extends Component {

	constructor(props){
    super(props);

    var sortterms = sortbyterms.map((term) =>
      <MenuItem key={term} value={term}>{term}</MenuItem>
    );

    var filtcityterms = filtercityterms.map((term) =>
      <MenuItem key={term} value={term}>{term}</MenuItem>
    );
    var filtdropoffterms = filterdropoffterms.map((term) =>
      <MenuItem key={term} value={term}>{term}</MenuItem>
    );
    var filtpickupterms = filterpickupterms.map((term) =>
      <MenuItem key={term} value={term}> {term}</MenuItem>
    );

		this.handleNextClick = this.handleNextClick.bind(this);
		this.handlePrevClick = this.handlePrevClick.bind(this);

    this.state = {
      page:1,
      maxpage:1,
      sortterms:sortterms,
      filtcityterms:filtcityterms,
      filtdropoffterms:filtdropoffterms,
      filtpickupterms:filtpickupterms,
    }
    this.fsdata = {
      sort_on:"None",
      filter_city:"None",
      filter_drop:"None",
      filter_pick:"None",
      search: "",
      sort_order:"ASC",
    }
	}

	get_page_url(index){
    var params = "";
    if(this.fsdata.sort_on !== "None"){
      params += "&sort="+this.fsdata.sort_on;
      params += "&sort_order="+this.fsdata.sort_order;
    }
    if(this.fsdata.filter_city !== "None")
      params += "&city_filter="+this.fsdata.filter_city;
    if(this.fsdata.filter_drop !== "None")
      params += "&dropoff_filter="+this.fsdata.filter_drop;
    if(this.fsdata.filter_pick !== "None")
      params += "&pickup_filter="+this.fsdata.filter_pick;
      params += "&search=" + format_string(this.fsdata.search);
		return "http://api.throwresponsibly.me/programs?size=12&page="+index+params;
	}

	get_page(index){
		let options = {
      method : 'GET',
      url: this.get_page_url(index),
    }
    request(options, function(error, response, body){
      if( this.state.error || error || response.statusCode !== 200){
        this.setState({error: true});
        return;
      }
			var json = JSON.parse(body)
			var result = json['result'];
      var max_page = json['max_page'];
			var sepItems = [];
			for(var i = 0; i < result.length; i++){
				let entry = result[i];
				var item =
				{
						"name":entry['name'],
						"image":entry['image_url'],
						"line1":"Type: "+entry['program_type'],
						"line2":"Dropoff: "+entry['dropoff'],
						"line3":"Pickup: "+entry['pickup'],
						"line4":"Phone: "+entry['phone'],
						"line5":"City: "+entry['city'],
						"id":entry['id'],
				}
				sepItems.push(item)
			}
			this.setState({items:sepItems,nextLink: "http://api.throwresponsibly.me"+result['next'] ,maxpage:max_page,ready:true})
    }.bind(this));
	}

	componentWillMount(){
		this.get_page(1);
	}


	handleNextClick(){
		var index = this.state.page+1 > this.state.maxpage ? this.state.page : this.state.page +1;
		if(index === this.state.page){
			return;
		}
	  this.setState({page: index})
		this.get_page(index);
	}
	handlePrevClick(){
		var index = this.state.page-1 < 1 ? this.state.page : this.state.page-1;
		if(index === this.state.page){
			return;
		}
	  this.setState({page: index})
		this.get_page(index);
	}

  apply = (label, value) =>{
    //set variables for api
    if(label === "City"){
      this.fsdata.filter_city=value;
    }else if (label === "Dropoff") {
      this.fsdata.filter_drop=value;
    }else if(label === "Pickup"){
      this.fsdata.filter_pick=value;
    }else if(label === "Sort By"){
      this.fsdata.sort_on=value.toLowerCase().split(" ")[0];
      var order = value.toLowerCase().split(" ")[1];
      if(order === "descending")
        this.fsdata.sort_order= "DESC"
      else {
        this.fsdata.sort_order= "ASC"
      }
    }
    this.get_page(1);
  };

  search = terms =>{
    //set variables for api
    this.fsdata.search = terms
    this.get_page(1);
  };

  render() {

		if(this.state.ready){
			return (
				<div className="model-root" id="model-root">
          <div className="selector-div" id="selectordiv">
            <div className="selector" id="sortselector">
              <SimpleSelect label="Sort By" items={this.state.sortterms} callback={this.apply}/>
            </div>
            <div className="selector" id="filtercselector">
              <SimpleSelect label="City" items={this.state.filtcityterms} callback={this.apply} className="selector"/>
            </div>
            <div className="selector" id="filterdselector">
              <SimpleSelect label="Dropoff" items={this.state.filtdropoffterms} callback={this.apply} className="selector"/>
            </div>
            <div className="selector" id="filterpselector">
              <SimpleSelect label="Pickup" items={this.state.filtpickupterms} callback={this.apply} className="selector"/>
            </div>
            <div className="selector">
              <SearchBar onRequestSearch={this.search} id="searchbar-comp"/>
            </div>
          </div>

  				<Model title="Programs" modelItems={this.state.items}/>
  				<div className="model-footer" id="modelfooter">
            <Button variant="contained" onClick={this.handlePrevClick} id="prevbutton">Prev</Button>
            <span className="model-pagination" id="paginationspan">Page {this.state.page} of {this.state.maxpage}</span>
            <Button variant="contained" onClick={this.handleNextClick} id="nextbutton">Next</Button>
          </div>
				</div>
			);
		}else{
			return("");
		}
	}
}

export default Programs;
