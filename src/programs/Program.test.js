import React from 'react';
import Enzyme from 'enzyme'
import {shallow} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Program from './Program';

Enzyme.configure({ adapter: new Adapter() });

it('renders without crashing', () => {
  shallow(<Program match={{params: {id: 1}}}/>);
});

it('defaults program', () => {
  const program =  shallow(<Program match={{params: {id: 1}}}/>);
  expect(program.text()).toEqual("<WithStyles(Typography) /><WithStyles(Typography) /><WithStyles(Typography) /><WithStyles(Typography) /><WithStyles(Typography) /><WithStyles(Typography) /><WithStyles(Typography) /><WithStyles(Typography) /><WithStyles(Typography) /><WithStyles(Typography) /><WithStyles(Typography) /><WithStyles(Grid) /><WithStyles(Typography) /><WithStyles(Grid) /><WithStyles(Typography) /><WithStyles(Grid) /><WithStyles(Typography) /><WithStyles(Grid) />");
});

it('program ready', () => {
  const program =  shallow(<Program match={{params: {id: 1}}}/>);
  expect(program.state().ready).toEqual(false);
});

it('program id', () => {
  const program =  shallow(<Program match={{params: {id: 1}}}/>);
  expect(program.state().id).toEqual(1);
});
