import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import ArrowDownward from '@material-ui/icons/ArrowDownward'

const Marker = ({ text }) => <div>{text}</div>;

class SimpleMap extends Component {
  static defaultProps = {
    zoom: 11
  };

  render() {

    var center = {
      lat: this.props.lat,
      lng: this.props.lng,
    };
    return (
      // Important! Always set the container height explicitly
      <div style={{ height: '100%', width: '100%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: "AIzaSyDzJdasK1PeyRi1tKf0bq6Tdm7ULgOZVdk" }}
          defaultCenter={center}
          defaultZoom={this.props.zoom}
        >
          <Marker
            lat={this.props.lat}
            lng={this.props.lng}
            text={<div lat={this.props.lat} lng={this.props.lng}>
              <ArrowDownward/>
            </div>}
          />
        </GoogleMapReact>
      </div>
    );
  }
}

export default SimpleMap;
