import React from 'react';
import Enzyme from 'enzyme'
import {shallow} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import ModelCard from './ModelCard';

Enzyme.configure({ adapter: new Adapter() });

it('renders without crashing', () => {
  shallow(<ModelCard modelItem={{name: "name", line1: "line1", line2: "line2", line3: "line3", line4: "line4", line5: "line5"}} title="Facilities"/>);
});

it('defaults modelCard', () => {
  const modelCard =  shallow(<ModelCard modelItem={{name: "name", line1: "line1", line2: "line2", line3: "line3", line4: "line4", line5: "line5"}} title="Facilities"/>);
  expect(modelCard.text()).toEqual("<WithStyles(Card) />");
});

it('defaults loaded', () => {
  const modelCard =  shallow(<ModelCard modelItem={{name: "name", line1: "line1", line2: "line2", line3: "line3", line4: "line4", line5: "line5"}} title="Facilities"/>);
  expect(modelCard.state().loaded).toEqual(true);
});

it('facility link_url', () => {
  const modelCard =  shallow(<ModelCard modelItem={{name: "name", line1: "line1", line2: "line2", line3: "line3", line4: "line4", line5: "line5"}} title="Facilities"/>);
  expect(modelCard.state().link_url).toEqual("/facility/undefined");
});

it('program link_url', () => {
  const modelCard =  shallow(<ModelCard modelItem={{name: "name", line1: "line1", line2: "line2", line3: "line3", line4: "line4", line5: "line5"}} title="Programs"/>);
  expect(modelCard.state().link_url).toEqual("/program/undefined");
});

it('item link_url', () => {
  const modelCard =  shallow(<ModelCard modelItem={{name: "name", line1: "line1", line2: "line2", line3: "line3", line4: "line4", line5: "line5"}} title="Items"/>);
  expect(modelCard.state().link_url).toEqual("/item/undefined");
});
