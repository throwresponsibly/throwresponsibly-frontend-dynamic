import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import './Model.css'

import { Link} from "react-router-dom";

import {Card, CardMedia, CardActionArea, CardContent} from '@material-ui/core';

function createMarkup(text) {
  return {__html: text};
}

class ModelCard extends Component {

  constructor(){
    super();
    this.state = {
      loaded: false,
    };
  }

  componentWillMount(){
    if(!this.state.loaded){
      switch(this.props.title){
        case "Facilities":
          this.setState({link_url :"/facility/"+this.props.modelItem['id']});
        break;
        case "Programs":
          this.setState({link_url : "/program/"+this.props.modelItem['id']});
        break;
        case "Items":
          this.setState({link_url : "/item/"+this.props.modelItem['id']});
        break;
        default:
        console.log("HOW DID WE GET HERE?")
        break;
      }
      this.setState({loaded:true})
    }
  }

  render() {
		return (
      <Card className="model-card" id="model-card" component={Link} to={this.state.link_url}>
        <CardActionArea>
          <CardMedia className="model-card-media" id="model-card-media" image={this.props.modelItem.image} />
          <CardContent className="model-card-content" id="model-card-content">
            <Typography variant="subheading"
                        className="model-card-name"
                        id="model-card-name"
                        noWrap
                        dangerouslySetInnerHTML={createMarkup(this.props.modelItem.name)}
            ></Typography>
            <Typography variant="body1"
                        id="line1"
                        dangerouslySetInnerHTML={createMarkup(this.props.modelItem.line1)}
            ></Typography>
            <Typography variant="body1"
                        id="line2"
                        dangerouslySetInnerHTML={createMarkup(this.props.modelItem.line2)}
            ></Typography>
            <Typography variant="body1"
                        id="line3"
                        dangerouslySetInnerHTML={createMarkup(this.props.modelItem.line3)}
            ></Typography>
            <Typography variant="body1"
                        id="line4"
                        dangerouslySetInnerHTML={createMarkup(this.props.modelItem.line4)}
            ></Typography>
            <Typography variant="body1"
                        id="line5"
                        dangerouslySetInnerHTML={createMarkup(this.props.modelItem.line5)}
            ></Typography>
          </CardContent>
        </CardActionArea>
      </Card>
		);
	}
}

export default ModelCard;
