import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import './Model.css'
import ModelCard from './ModelCard'

class Model extends Component {

  render() {
		return (
		  <div className="model-root" id="model-root">
        <div className="model-header" id="model-header">
          <Typography variant="h2" id="title">{this.props.title}</Typography>
        </div>
        <div className="model-body" id="model-body">
          {this.props.modelItems.map((modelItem) =>
            <ModelCard modelItem={modelItem} title={this.props.title} key={modelItem['id']}/>
          )}
        </div>

		  </div>
		);
	}
}

export default Model;
