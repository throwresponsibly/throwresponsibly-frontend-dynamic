import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

const styles = theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap"
  },
  selector: {
    margin: theme.spacing.unit,

  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  },
  form:{
    margin: 2 * theme.spacing.unit,
    minWidth: 200,
  }
});

class SimpleSelect extends React.Component {

  state = {
    value: "None",
  };

  handleChange = event => {
    this.setState({ value: event.target.value });
    this.props.callback(this.props.label, event.target.value);
  };

  render() {
    return (
        <FormControl className="form" id="form">
          <InputLabel shrink htmlFor={this.props.label + "-label-placeholder"}
              id="input-label"
              >
            {this.props.label}
          </InputLabel>
          <Select
            value={this.state.value}
            onChange={this.handleChange}
            input={
              <Input
                name={this.props.label}
                id={this.props.label + "-label-placeholder"}
              />
            }
            displayEmpty
            name={this.props.label}
            className="selectEmpty"
          >
            <MenuItem value="None">
              <em>None</em>
            </MenuItem>
            {this.props.items}
          </Select>
        </FormControl>
    );
  }
}

export default withStyles(styles)(SimpleSelect);
