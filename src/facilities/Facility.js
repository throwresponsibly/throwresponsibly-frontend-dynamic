import React, { Component } from 'react';
import {Typography, Grid, Card, CardMedia} from '@material-ui/core'
import { Link} from "react-router-dom";
import SimpleMap from '../model/SimpleMap'
import './Facility.css'
var request = require('request');

class Facility extends Component{

  constructor(props){
    super(props);
    this.state={
      ready:false,
      id:props.match.params.id,
    }
  }

  componentWillMount(){
    let options = {
			method : 'GET',
			url: "http://api.throwresponsibly.me/facilities/"+this.state.id,
		}
		request(options, function(error, response, body){
			if( this.state.error || error || response.statusCode !== 200){
				this.setState({error: true});
				return;
			}
			var json = JSON.parse(body)

      this.setState({
        ready:true,
        name:json['name'],
        image_url:json['image_url'],
        dropoff: json['dropoff'] ? "Yes":"No",
        pickup: json['pickup'] ? "Yes":"No",
        phone: json['phone'],
        url: json['url'],
        city: json['city'],
        address: json['address'],
        type: json['facility_type'],
        items: json['items'].slice(0,3),
        programs: json['programs'].slice(0,3),
        lat: json['lat'],
        lng: json['lng'],
      });
      this.forceUpdate();
		}.bind(this));
  }

  render(){
    let items = <div></div>;
    let programs = <div></div>;
    let map=<div></div>
    if (this.state.ready){
      items = Object.entries(this.state.items).map((item) =>(
        <Grid item key={item[1]['id']}>
          <Card component={Link} to={"/item/"+item[1]['id']} className="model-card">
            <CardMedia className="model-card-media" image={item[1]['img_url']} />
            <Typography>{item[1]['name']}</Typography>
          </Card>
        </Grid>
      ));

      programs = Object.entries(this.state.programs).map((program) =>(
        <Grid item key={program[1]['id']}>
          <Card component={Link} to={"/program/"+program[1]['id']} className="model-card">
            <CardMedia className="model-card-media" image={program[1]['image_url']} />
            <Typography>{program[1]['name']}</Typography>
          </Card>
        </Grid>
      ));
      map = <SimpleMap name={this.state.name} lat={this.state.lat} lng={this.state.lng}/>
    }
      return(
        <div className="itemWrapper" id="itemWrapper">
          <div className="titlediv" id="titlediv">
            <Typography variant="h2" id="name">{this.state.name}</Typography>
          </div>
          <div className="imagewrapperdiv" id="imagewrapperdiv">
            <div className="imagediv" id="imagediv">
              <img src={this.state.image_url} className="facilityImage" alt="Img CORS protected"></img>
            </div>
          </div>
          <div className="locationdiv" id="locationdiv">
          <Typography variant="h5">Location</Typography>
          <Typography variant="body1">{this.state.address}</Typography>
            <div className="mapdiv" id="mapdiv">
              {map}
            </div>
          </div>
          <div className="propsdiv" id="propsdiv">
            <div className="propstextdiv" id="propstextdiv">
              <Typography variant="h4">Stats</Typography>
              <Typography variant="body1"><b>Type:</b> {this.state.type}</Typography>
              <Typography variant="body1"><b>Dropoff:</b> {this.state.dropoff}</Typography>
              <Typography variant="body1"><b>Pickup:</b> {this.state.pickup}</Typography>
              <Typography variant="body1"><b>Phone Number:</b> {this.state.phone}</Typography>
              <Typography variant="body1"><b>Website:</b> <a href={this.state.url}>{this.state.url}</a></Typography>
              <Typography variant="body1"><b>City:</b> {this.state.city}</Typography>
            </div>
          </div>



          <div className="itemsdiv" id="itemsdiv">
            <Typography variant="h5">Recyclable Items at this Location:</Typography>
            <Grid container justify="center">
              {items}
            </Grid>
          </div>
          <div className="programsdiv" id="programsdiv">
            <Typography variant="h5">Alternate Programs for these Items:</Typography>
            <Grid container justify="center">
              {programs}
            </Grid>
          </div>
        </div>
        );
  }
}

export default Facility;
