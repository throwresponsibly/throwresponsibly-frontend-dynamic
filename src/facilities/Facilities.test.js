import React from 'react';
import Enzyme from 'enzyme'
import {shallow} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Facilities from './Facilities';

Enzyme.configure({ adapter: new Adapter() });

it('renders without crashing', () => {
  shallow(<Facilities />);
});

it('defaults to empty text since state is set to not ready', () => {
  const facilities =  shallow(<Facilities />);
  expect(facilities.text()).toEqual('');
});

it('defaults to page 1', () => {
  const facilities =  shallow(<Facilities/>);
  expect(facilities.state().page).toEqual(1);
});

it('num of sort terms', () => {
  const facilities =  shallow(<Facilities/>);
  expect(facilities.state().sortterms.length).toEqual(4);
});

it('num of city terms', () => {
  const facilities =  shallow(<Facilities/>);
  expect(facilities.state().filtcityterms.length).toEqual(383);
});

it('num of drop off terms', () => {
  const facilities =  shallow(<Facilities/>);
  expect(facilities.state().filtdropoffterms.length).toEqual(2);
});

it('num of pick up terms', () => {
  const facilities =  shallow(<Facilities/>);
  expect(facilities.state().filtpickupterms.length).toEqual(2);
});
