import React from 'react';
import Enzyme from 'enzyme'
import {shallow} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Facility from './Facility';

Enzyme.configure({ adapter: new Adapter() });

it('renders without crashing', () => {
  shallow(<Facility match={{params: {id: 1}}}/>);
});

it('defaults facility', () => {
  const facility =  shallow(<Facility match={{params: {id: 1}}}/>);
  expect(facility.text()).toEqual("<WithStyles(Typography) /><WithStyles(Typography) /><WithStyles(Typography) /><WithStyles(Typography) /><WithStyles(Typography) /><WithStyles(Typography) /><WithStyles(Typography) /><WithStyles(Typography) /><WithStyles(Typography) /><WithStyles(Typography) /><WithStyles(Typography) /><WithStyles(Grid) /><WithStyles(Typography) /><WithStyles(Grid) /><WithStyles(Typography) /><WithStyles(Grid) /><WithStyles(Typography) /><WithStyles(Grid) />");
});

it('facility ready', () => {
  const facility =  shallow(<Facility match={{params: {id: 1}}}/>);
  expect(facility.state().ready).toEqual(false);
});
it('facility id', () => {
  const facility =  shallow(<Facility match={{params: {id: 1}}}/>);
  expect(facility.state().id).toEqual(1);
});
