import React, { Component } from 'react';
import Model from '../model/Model';
import {Button,MenuItem} from '@material-ui/core';
import SimpleSelect from '../model/SimpleSelect'
import SearchBar from '../search/SearchBar';
import './Facilities.css'

var request = require('request')

var sortbyterms = [
  "Name Ascending",
  "Name Descending",
  "City Ascending",
  "City Descending",
];

var filtercityterms = ["Aberdeen","Agawam","Aiea","Allston","Altavista","Anchorage",
"Andover","Angola","Antioch","Arlington","Arlington Heights","Arvada","Ashland",
"ASSONET","Athens","Atlanta","Auburn","Aurora","Austin","Avon","Baltimore","Barrington",
"Bauxite","Beckley","Bedford","Belle Vernon","Belleville","Bellevue","Bellevue ",
"Bell Gardens","Belmont","Bethel Park","Bethune","Bettendorf","Billings","Biloxi",
"Bloomfield","Blue Island","Boston","Boulder","Braintree","Brentwood","Brigham City",
"Brighton","Bristol","Brockton","Broken Arrow","Bronson","Bronx","Brookfield",
"Brooklyn","Broomfield","Brunswick","Bryant","Burbank","Butner","Cahokia","Cambridge",
"Camden","Canton","Cantonment","Carmel","Cassatt","Catonsville","Cedar Grove","Cedar Park",
"Central Manchester","Charlestown","Chicago","Church Hill","Cleveland","Coldwater",
"Columbia","Commerce","Commerce City","Compton","Conway","Coraopolis","Cotton",
"Cranston","Creedmoor","Cromwell","Dartmouth","Davenport","Decatur","Dedham","Delafield",
"Denver","Derby","Diberville","D'Iberville","Dover","Downers Grove","Downey","Duluth",
"Durham","Eagle River","East Greenwich","East Hanover","East Hartford","Eastpoint","East Point",
"East Providence","East St Louis","Elgin","Elizabeth","Elkridge","Ellenwood","El Monte",
"Elmwood Park","Enfield","Englewood","Essex","Euclid","Evanston","Everett","Export",
"Fairfield","Fairhaven","Fair Haven","Fairview Heights","Fall River","Falmouth","Fayetteville",
"Federal Heights","Felton","Fishers","Forest","Forest Park","Franklin","Frederica",
"Freeport","Gardena","Garfield Heights","Geneseo","Gilford","Glastonbury","Glen Burnie",
"Glendale","Goodlettsville","Grafton","Grand Island","Granite City","Greendale",
"Greenfield","Greenwood","Gretna","Gulfport","Gurley","Hanover","Hartford","Hartland",
"Harvey","Hastings","Hazard","Hazelwood","Heidelberg","Henderson","Hendersonville",
"Hermitage","Holderness","Homestead","Honolulu","Houston","Huntington Park","Huntsville",
"Indianapolis","Indianapolis ","Inglewood","Irwin","Irwindale","Issaquah","Jackson",
"Jacksonville","Jamaica Plain","Jeannette","Jefferson","Jenks","Jericho","Jersey City",
"Johnston","Jonesboro","Jonesville","Kaneohe","Keasbey","Kenner","Kent","Kershaw",
"Killington","Kingman","Kirkland","Kunia","Lafayette","Lakewood","Laurel","La Vergne",
"Lebanon","Lewiston","Lincolnwood","Linthicum","Lithia Springs","Little Creek","Little Rock",
"Livingston","Logan","Long Island City","Lonoke","Los Angeles","Lugoff","Lutherville-Timonium",
"Lynchburg","Macedonia","Madison","Madison Heights","Magnolia","Maize","Manchester","Manhattan",
"Marbleton","Marlborough","Marrero","Maumelle","Mayfield Heights","McDonough","Medford","Melrose Park",
"Menomonee Falls","Meredith","Meriden","Mesa","Metairie","Middletown","Milford","Millington","Milwaukee",
"Mine Hill","Moline","Monroeville","Morrow","Mountain View","Mount Juliet","Nashville","Newark",
"New Bedford","New Britain","New Hampton","New Hope","New Hyde Park","Newington",
"New Market","New Orleans","Newport","Newton","New York","Norridge","Northglenn","North Kingstown",
"North Little Rock","North Olmsted","North Riverside","North Royalton","Nottingham",
"Oak Creek","Oak Hill","Orange Park","Owasso","Oxford","Paramount","Park City","Parkville",
"Parma","Parsippany","Pascagoula","Passaic","Peace Dale","Pearl City","Pendleton",
"Pensacola","Phoenix","Pikesville","Pinedale","Pittsburgh","Plymouth","Portland","Preston",
"Providence","Racine","Randolph","Redmond","Reno","Renton","Revere","Richmond Heights",
"Ridgely","Ridgeway","Riverdale","Rockaway","Rock Island","Rocky Hill","Rosedale","Round Rock",
"Roxboro","Roxbury","Rutland","Saddle Brook","Saint Louis","Sand Springs","Sandy Springs","Santa Fe",
"santa fe springs","Santa Maria","Saugus","Scottsdale","Seattle","Secaucus","Seekonk",
"Shoreline","Smith Mills","Smyrna","Solon","Somerset","Somerville","South Boston","South Gate",
"Southington","Southport","South Portland","Sparks","Springfield","Springfield Township",
"Staten Island","St Louis","St. Louis","Stone Mountain","Strongsville","Sturgis",
"Sumter","Tacoma","Tempe","Tilton","Topsham","Townsend","Towson","Tucker","Tukwila",
"Tulsa","Union","Union City","Union Township","Unon","Valley Park","Valley Stream",
"Vashon","Vauxhall","Vernon","Virginia City","Waipahu","Wakefield","Warwick",
"Watertown","Waukesha","Wauwatosa","Wayne","Webster Groves","Westbury","West Hartford",
"West Hempstead","West Lebanon","West Milwaukee","Westminster","West Orange","West Paterson",
"West Roxbury","West Simsbury","Wheat Ridge","White Marsh","Wichita","Winchester",
"Windham","Windsor","Windsor Mill","Wolcott","Woodmere"];

var filterdropoffterms = [
  "True",
	"False"
];
var filterpickupterms = [
	"True",
	"False"
];

function format_string(str){
  return str.replace(/ /g,"%20")
}

class Facilities extends Component {

	constructor(){
		super();

    this.handleNextClick = this.handleNextClick.bind(this);
    this.handlePrevClick = this.handlePrevClick.bind(this);

    var sortterms = sortbyterms.map((term) =>
			<MenuItem key={term} value={term}>{term}</MenuItem>
		);

		var filtcityterms = filtercityterms.map((term) =>
			<MenuItem key={term} value={term}>{term}</MenuItem>
		);
		var filtdropoffterms = filterdropoffterms.map((term) =>
			<MenuItem key={term} value={term}>{term}</MenuItem>
		);
		var filtpickupterms = filterpickupterms.map((term) =>
			<MenuItem key={term} value={term}>{term}</MenuItem>
		);

    this.state = {
      page:1,
      maxpage:1,
      sortterms:sortterms,
      filtcityterms:filtcityterms,
      filtdropoffterms:filtdropoffterms,
      filtpickupterms:filtpickupterms,
    };

    this.fsdata = {
      sort_on:"None",
      filter_city:"None",
      filter_drop:"None",
      filter_pick:"None",
      search: "",
      sort_order:"ASC",
    }
	}

	get_page_url(index){
    var params = "";
    if(this.fsdata.sort_on !== "None"){
      params += "&sort="+this.fsdata.sort_on;
      params += "&sort_order="+this.fsdata.sort_order;
    }
    if(this.fsdata.filter_city !== "None")
      params += "&city_filter="+this.fsdata.filter_city;
    if(this.fsdata.filter_drop !== "None")
      params += "&dropoff_filter="+this.fsdata.filter_drop;
    if(this.fsdata.filter_pick !== "None")
      params += "&pickup_filter="+this.fsdata.filter_pick;
      params += "&search=" + format_string(this.fsdata.search);
		return "http://api.throwresponsibly.me/facilities?size=12&page="+index+params;
	}

	get_page(index){
		let options = {
      method : 'GET',
      url: this.get_page_url(index),
    }
    request(options, function(error, response, body){
      if( this.state.error || error || response.statusCode !== 200){
        this.setState({error: true});
        return;
      }
			var json = JSON.parse(body)
			var result = json['result'];
      var max_page = json['max_page'];
			var sepItems = [];
			for(var i = 0; i < result.length; i++){
				let entry = result[i];
				var item =
				{
						"name":entry['name'],
						"image":entry['image_url'],
						"line1":"Type: "+entry['facility_type'],
						"line2":"Dropoff: "+entry['dropoff'],
						"line3":"Pickup: "+entry['pickup'],
						"line4":"Phone: "+entry['phone'],
						"line5":"City: "+entry['city'],
						"id":entry['id'],
				}
				sepItems.push(item)
			}
			this.setState({items:sepItems,nextLink: "http://api.throwresponsibly.me"+result['next'], maxpage:max_page ,ready:true})
    }.bind(this));
	}

	componentWillMount(){
		this.get_page(1);
	}


	handleNextClick(){
		var index = this.state.page+1 > this.state.maxpage ? this.state.page : this.state.page +1;
		if(index === this.state.page){
			return;
		}
	  this.setState({page: index})
		this.get_page(index);
	}
	handlePrevClick(){
		var index = this.state.page-1 < 1 ? this.state.page : this.state.page-1;
		if(index === this.state.page){
			return;
		}
	  this.setState({page: index})
		this.get_page(index);
	}

  apply = (label, value) =>{
    //set variables for api
    if(label === "City"){
      this.fsdata.filter_city=value;
    }else if (label === "Dropoff") {
      this.fsdata.filter_drop=value;
    }else if(label === "Pickup"){
      this.fsdata.filter_pick=value;
    }else if(label === "Sort By"){
      this.fsdata.sort_on=value.toLowerCase().split(" ")[0];
      var order = value.toLowerCase().split(" ")[1];
      if(order === "descending")
        this.fsdata.sort_order= "DESC"
      else {
        this.fsdata.sort_order= "ASC"
      }
    }
    this.get_page(1);
  };

  search = terms =>{
    //set variables for api
    this.fsdata.search = terms
    this.get_page(1);
  };

  render() {

		if(this.state.ready){
			return (
				<div className="model-root" id="model-root">
  				<div className="selector-div" id="selector-div">
            <div className="selector" id="sortselector">
              <SimpleSelect label="Sort By" items={this.state.sortterms} callback={this.apply}/>
            </div>
            <div className="selector" id="filtercselector">
              <SimpleSelect label="City" items={this.state.filtcityterms} callback={this.apply} className="selector"/>
            </div>
            <div className="selector" id="filterdselector">
              <SimpleSelect label="Dropoff" items={this.state.filtdropoffterms} callback={this.apply} className="selector"/>
            </div>
            <div className="selector" id="filterpselector">
              <SimpleSelect label="Pickup" items={this.state.filtpickupterms} callback={this.apply} className="selector"/>
            </div>
            <div className="selector">
              <SearchBar onRequestSearch={this.search} id="searchbar-comp"/>
            </div>
  				</div>
  				<Model title="Facilities" modelItems={this.state.items}/>
  				<div className="model-footer" id="model-footer">
            <Button variant="contained" onClick={this.handlePrevClick} id="prevbutton">Prev</Button>
            <span className="model-pagination" id="model-pagination">Page {this.state.page} of {this.state.maxpage}</span>
            <Button variant="contained" onClick={this.handleNextClick} id="nextbutton">Next</Button>
          </div>
				</div>
			);
		}else{
			return("");
		}
	}
}

export default Facilities;
