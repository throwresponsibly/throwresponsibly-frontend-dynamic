import React, { Component } from 'react';
import {Typography,Button} from '@material-ui/core';
import {Link} from "react-router-dom";
import PieChart from './PieChart';
import BarChart from './BarChart';
import './Visual.css';
const facility_visual =
[
{name:"Non-Profit",value:214,percent:16.07},
{name:"Regional Retail",value:6,percent:.45},
{name:"Commercial",value:322,percent:24.19},
{name:"Household Hazardous Waste",value:13,percent:.97},
{name:"National Retail",value:585,percent:43.95},
{name:"Government",value:173,percent:12.99},
{name:"Unstaffed Bin",value:18,percent:1.35}
];

const program_visual =
[
{name:"Mail-In",value:63,percent:51.63},
{name:"Household Hazardous Waste",value:1,percent:.81},
{name:"Commercial Curbside",value:33,percent:27.04},
{name:"Other",value:23,percent:18.85},
{name:"Resource Contact Information",value:2,percent:1.63},
];

const item_visual =
[
{name:"#1 Plastic Bags",value:(101/1648*100)},
{name:"#1 Plastic Beverage Bottles",value:(101/1648*100)},
{name:"#1 Plastic Clamshells",value:(101/1648*100)},
{name:"#1 Plastic Film",value:(101/1648*100)},
{name:"#1 Plastic Trays",value:(101/1648*100)},
{name:"#1 Rigid Plastics",value:(101/1648*100)},
{name:"#2 Plastic Bags",value:(101/1648*100)},
{name:"#2 Plastic Bottles",value:(101/1648*100)},
{name:"#4 Plastic Film",value:(101/1648*100)},
{name:"#2 Plastic Caps",value:(101/1648*100)},
{name:"#2 Plastic Clamshells",value:(101/1648*100)},
{name:"#2 Plastic Film",value:(101/1648*100)},
{name:"#2 Plastic Jugs - Clear",value:(101/1648*100)},
{name:"#2 Plastic Jugs - Colored",value:(101/1648*100)},
{name:"#2 Rigid Plastics",value:(101/1648*100)},
{name:"#3 Plastic Bags",value:(101/1648*100)},
{name:"#3 Plastic Bottles",value:(101/1648*100)},
{name:"#3 Plastic Film",value:(101/1648*100)},
{name:"#5 Plastic Clamshells",value:(101/1648*100)},
{name:"#3 Rigid Plastics",value:(101/1648*100)},
{name:"#4 Flexible Plastics",value:(101/1648*100)},
{name:"#4 Plastic Bags",value:(101/1648*100)},
{name:"#4 Plastic Bottles",value:(101/1648*100)},
{name:"#4 Rigid Plastics",value:(101/1648*100)},
{name:"#5 Plastic Bags",value:(101/1648*100)},
{name:"#5 Plastic Bottles",value:(108/1648*100)},
{name:"#5 Plastic Caps",value:(101/1648*100)},
{name:"#5 Plastic Cups",value:(101/1648*100)},
{name:"#5 Plastic Film",value:(101/1648*100)},
{name:"#5 Rigid Plastics",value:(101/1648*100)},
{name:"#6 Plastic - Expanded",value:(101/1648*100)},
{name:"#6 Plastic Bags",value:(101/1648*100)},
{name:"#6 Plastic Bottles",value:(101/1648*100)},
{name:"#6 Plastic Cups",value:(101/1648*100)},
{name:"#6 Plastic Film",value:(101/1648*100)},
{name:"#6 Plastic Peanuts",value:(101/1648*100)},
{name:"#6 Rigid Plastics",value:(101/1648*100)},
{name:"#7 Plastic Bags",value:(101/1648*100)},
{name:"#7 Plastic Bottles",value:(101/1648*100)},
{name:"#7 Plastic Film",value:(101/1648*100)},
{name:"#7 Rigid Plastics",value:(101/1648*100)},
{name:"Acids",value:(208/1648*100)},
{name:"Adhesives",value:(208/1648*100)},
{name:"Adult Toys",value:(236/1648*100)},
{name:"Aluminum Foil",value:(708/1648*100)},
{name:"Aerosol Cans - Empty",value:(505/1648*100)},
{name:"Aerosol Cans - Full",value:(458/1648*100)},
{name:"Air Conditioners",value:(713/1648*100)},
{name:"Alkaline Batteries",value:(179/1648*100)},
{name:"Aluminum Beverage Cans",value:(708/1648*100)},
{name:"Aluminum Food Cans",value:(708/1648*100)},
{name:"Aluminum Lids",value:(560/1648*100)},
{name:"Aluminum Pie Plates",value:(708/1648*100)},
{name:"Aluminum Scrap",value:(505/1648*100)},
{name:"Aluminum Trays",value:(708/1648*100)},
{name:"Ammunition",value:(208/1648*100)},
{name:"Antifreeze",value:(337/1648*100)},
{name:"Arts and Crafts Supplies",value:(236/1648*100)},
{name:"Asbestos",value:(208/1648*100)},
{name:"Asphalt",value:(295/1648*100)},
{name:"Audio Equipment",value:(1143/1648*100)},
{name:"Auto Bodies",value:(634/1648*100)},
{name:"Auto Parts",value:(634/1648*100)},
{name:"Baby Products",value:(236/1648*100)},
{name:"Barbeque Grills",value:(741/1648*100)},
{name:"Bicycles",value:(741/1648*100)},
{name:"Bike Tires",value:(320/1648*100)},
{name:"Blu-Ray Players",value:(1143/1648*100)},
{name:"Blue Glass Beverage Containers",value:(55/1648*100)},
{name:"Blue Glass Containers",value:(55/1648*100)},
{name:"Blueprints",value:(354/1648*100)},
{name:"Booklets",value:(354/1648*100)},
{name:"Boomboxes",value:(1143/1648*100)},
{name:"Brake Fluid",value:(337/1648*100)},
{name:"Branches",value:(74/1648*100)},
{name:"Brass",value:(505/1648*100)},
{name:"Brick",value:(295/1648*100)},
{name:"Brochures",value:(354/1648*100)},
{name:"Brown Glass Containers",value:(55/1648*100)},
{name:"Brush",value:(74/1648*100)},
{name:"Button Cell Batteries",value:(330/1648*100)},
{name:"Cables",value:(1143/1648*100)},
{name:"Calculators",value:(1143/1648*100)},
{name:"Candy Wrappers",value:(236/1648*100)},
{name:"Car Batteries",value:(414/1648*100)},
{name:"Car Fluids",value:(292/1648*100)},
{name:"Cardstock",value:(354/1648*100)},
{name:"Carpet",value:(538/1648*100)},
{name:"Carpet Padding",value:(531/1648*100)},
{name:"Cassette Players",value:(1143/1648*100)},
{name:"Cassette Tapes",value:(1143/1648*100)},
{name:"Catalogs",value:(354/1648*100)},
{name:"CD Cases",value:(337/1648*100)},
{name:"CD Players",value:(1143/1648*100)},
{name:"CDs",value:(1523/1648*100)},
{name:"Ceiling Tiles",value:(295/1648*100)},
{name:"Cell Phone Accessories",value:(1143/1648*100)},
{name:"Cell Phones",value:(1523/1648*100)},
{name:"Ceramic Tile",value:(295/1648*100)},
{name:"Christmas Trees",value:(89/1648*100)},
{name:"CFLs",value:(255/1648*100)},
{name:"Chemistry Sets",value:(444/1648*100)},
{name:"Chip Bags",value:(236/1648*100)},
{name:"Chipboard",value:(354/1648*100)},
{name:"Cigarettes",value:(236/1648*100)},
{name:"Clear Glass Beverage Containers",value:(55/1648*100)},
{name:"Clear Glass Containers",value:(55/1648*100)},
{name:"Clothing",value:(236/1648*100)},
{name:"Compostable Plastics",value:(101/1648*100)},
{name:"Computer Peripherals - External",value:(1143/1648*100)},
{name:"Computer Peripherals - Internal",value:(1143/1648*100)},
{name:"Concrete",value:(295/1648*100)},
{name:"Construction Debris",value:(207/1648*100)},
{name:"Construction Materials",value:(295/1648*100)},
{name:"Construction Paper",value:(354/1648*100)},
{name:"Contaminated Soil",value:(415/1648*100)},
{name:"Contaminated Wood",value:(415/1648*100)},
{name:"Controlled Substances",value:(444/1648*100)},
{name:"Cooking Oil",value:(451/1648*100)},
{name:"Cookware",value:(741/1648*100)},
{name:"Eyeglasses",value:(236/1648*100)},
{name:"Green Glass Beverage Containers",value:(55/1648*100)},
{name:"Copper",value:(505/1648*100)},
{name:"Corks",value:(236/1648*100)},
{name:"Corrugated Cardboard",value:(546/1648*100)},
{name:"Crayons",value:(236/1648*100)},
{name:"CRT Computer Monitors",value:(1143/1648*100)},
{name:"CRT Televisions",value:(1143/1648*100)},
{name:"Curling Irons",value:(1648/1648*100)},
{name:"Dehumidifiers",value:(1648/1648*100)},
{name:"Desktop Computers",value:(1143/1648*100)},
{name:"Digital Cameras",value:(1143/1648*100)},
{name:"Dirt",value:(369/1648*100)},
{name:"Dishwashers",value:(505/1648*100)},
{name:"Doors",value:(207/1648*100)},
{name:"Motor Oil",value:(292/1648*100)},
{name:"Drink Boxes",value:(546/1648*100)},
{name:"Drink Pouches",value:(236/1648*100)},
{name:"DVD Players",value:(1143/1648*100)},
{name:"DVDs",value:(1523/1648*100)},
{name:"Electronic Servers",value:(1143/1648*100)},
{name:"Engine Degreasers",value:(292/1648*100)},
{name:"Envelopes",value:(546/1648*100)},
{name:"Explosives",value:(208/1648*100)},
{name:"Fabric",value:(236/1648*100)},
{name:"Ferrous Metals",value:(708/1648*100)},
{name:"Fertilizers",value:(208/1648*100)},
{name:"Fire Extinguishers",value:(713/1648*100)},
{name:"Flip-Flops",value:(236/1648*100)},
{name:"Floppy Disks",value:(1523/1648*100)},
{name:"Fluorescent Tubes",value:(255/1648*100)},
{name:"Freezers",value:(949/1648*100)},
{name:"Fungicides",value:(208/1648*100)},
{name:"Game Consoles",value:(1143/1648*100)},
{name:"Garden Tools",value:(842/1648*100)},
{name:"Gas Cylinders",value:(713/1648*100)},
{name:"Gas/Oil Mixture",value:(292/1648*100)},
{name:"Gasoline",value:(337/1648*100)},
{name:"Gift Bags",value:(280/1648*100)},
{name:"Gift Boxes",value:(398/1648*100)},
{name:"Hard Drives",value:(1523/1648*100)},
{name:"Glassine",value:(354/1648*100)},
{name:"Glue Sticks",value:(337/1648*100)},
{name:"GPS Systems",value:(1143/1648*100)},
{name:"Grass Clippings",value:(74/1648*100)},
{name:"Green Glass Containers",value:(55/1648*100)},
{name:"Greeting Cards",value:(398/1648*100)},
{name:"Gypsum Drywall",value:(295/1648*100)},
{name:"Hair",value:(236/1648*100)},
{name:"Hair Dye",value:(444/1648*100)},
{name:"Hair Spray",value:(444/1648*100)},
{name:"Halogen Bulbs",value:(236/1648*100)},
{name:"Hardcover Books",value:(354/1648*100)},
{name:"Hardware",value:(712/1648*100)},
{name:"Hay",value:(45/1648*100)},
{name:"Heaters",value:(741/1648*100)},
{name:"Herbicides",value:(208/1648*100)},
{name:"HHW",value:(208/1648*100)},
{name:"Holiday Greens",value:(89/1648*100)},
{name:"Home Electronics",value:(1143/1648*100)},
{name:"Household Cleaners",value:(444/1648*100)},
{name:"Household Furniture",value:(443/1648*100)},
{name:"Hoverboards",value:(1143/1648*100)},
{name:"Humidifiers",value:(1648/1648*100)},
{name:"Hydraulic Fluid",value:(292/1648*100)},
{name:"Office Furniture",value:(223/1648*100)},
{name:"Incandescent Lightbulbs",value:(236/1648*100)},
{name:"Inkjet Cartridges",value:(1523/1648*100)},
{name:"Innertubes",value:(84/1648*100)},
{name:"Insecticides",value:(208/1648*100)},
{name:"Kerosene",value:(491/1648*100)},
{name:"Kraft Paper",value:(354/1648*100)},
{name:"Lacquer",value:(217/1648*100)},
{name:"Laptop Computers",value:(1143/1648*100)},
{name:"Latex Paint",value:(305/1648*100)},
{name:"Lawnmowers",value:(505/1648*100)},
{name:"LCD Computer Monitors",value:(1523/1648*100)},
{name:"LCD Televisions",value:(1523/1648*100)},
{name:"Lead Paint Chips",value:(217/1648*100)},
{name:"Leaves",value:(74/1648*100)},
{name:"Lice Shampoo",value:(444/1648*100)},
{name:"Light Fixtures",value:(207/1648*100)},
{name:"Lighter Fluid",value:(444/1648*100)},
{name:"Lighting Ballasts",value:(415/1648*100)},
{name:"Linens",value:(236/1648*100)},
{name:"Linoleum",value:(295/1648*100)},
{name:"Lithium Batteries",value:(122/1648*100)},
{name:"Lithium-ion Batteries",value:(387/1648*100)},
{name:"Lumber",value:(207/1648*100)},
{name:"Magazines",value:(546/1648*100)},
{name:"Mail",value:(354/1648*100)},
{name:"Manila Folders",value:(354/1648*100)},
{name:"Manure",value:(45/1648*100)},
{name:"Marine Batteries",value:(206/1648*100)},
{name:"Mattresses",value:(443/1648*100)},
{name:"Medical Equipment - Handheld",value:(1351/1648*100)},
{name:"Medical Equipment - Large",value:(1570/1648*100)},
{name:"Medical Sharps",value:(208/1648*100)},
{name:"Medication Containers",value:(101/1648*100)},
{name:"Medications",value:(208/1648*100)},
{name:"Mercury Thermostats",value:(255/1648*100)},
{name:"Metal Clothes Hangers",value:(741/1648*100)},
{name:"Musical Instruments - Non-metal",value:(236/1648*100)},
{name:"Metal Paint Cans",value:(708/1648*100)},
{name:"Metal Tags",value:(708/1648*100)},
{name:"Microwaves",value:(1143/1648*100)},
{name:"Milk and Juice Cartons",value:(354/1648*100)},
{name:"Office Supplies",value:(236/1648*100)},
{name:"Mixed Paper",value:(546/1648*100)},
{name:"Motor Oil Containers",value:(230/1648*100)},
{name:"MP3 Players",value:(1143/1648*100)},
{name:"Musical Instruments - Metal",value:(741/1648*100)},
{name:"Nail Polish",value:(444/1648*100)},
{name:"Plastic Plant Materials",value:(337/1648*100)},
{name:"Nail Polish Removers",value:(208/1648*100)},
{name:"Neon Lights",value:(207/1648*100)},
{name:"Newspaper",value:(546/1648*100)},
{name:"Newspaper Inserts",value:(354/1648*100)},
{name:"Nickel-cadmium Batteries",value:(387/1648*100)},
{name:"Nickel-metal Hydride Batteries",value:(330/1648*100)},
{name:"Nickel-zinc Batteries",value:(330/1648*100)},
{name:"Nonferrous Metals",value:(708/1648*100)},
{name:"Office Machines",value:(1523/1648*100)},
{name:"Office Paper",value:(546/1648*100)},
{name:"Oil Filters",value:(337/1648*100)},
{name:"Oil-Based Paint",value:(217/1648*100)},
{name:"Organic Food Waste",value:(74/1648*100)},
{name:"Ornaments",value:(280/1648*100)},
{name:"Pagers",value:(1143/1648*100)},
{name:"Paint Strippers",value:(217/1648*100)},
{name:"Paint Thinners",value:(305/1648*100)},
{name:"Pallets",value:(311/1648*100)},
{name:"Paper Bags",value:(354/1648*100)},
{name:"Paper Cups",value:(546/1648*100)},
{name:"Paper Egg Cartons",value:(354/1648*100)},
{name:"Paper Labels",value:(546/1648*100)},
{name:"Paper Shredders",value:(1143/1648*100)},
{name:"Paper Sleeves",value:(546/1648*100)},
{name:"Stone",value:(295/1648*100)},
{name:"Paper Tubes",value:(354/1648*100)},
{name:"Paperback Books",value:(546/1648*100)},
{name:"Plastic Bottle Accessories",value:(101/1648*100)},
{name:"Paperboard",value:(546/1648*100)},
{name:"Perfumes",value:(208/1648*100)},
{name:"Pesticide Containers",value:(101/1648*100)},
{name:"Pesticides",value:(208/1648*100)},
{name:"Phone Books",value:(546/1648*100)},
{name:"Photographic Chemicals",value:(208/1648*100)},
{name:"Pipe",value:(505/1648*100)},
{name:"Pizza Boxes",value:(590/1648*100)},
{name:"Plastic Buckets",value:(337/1648*100)},
{name:"Plastic Cards",value:(101/1648*100)},
{name:"Stoves",value:(505/1648*100)},
{name:"Plastic Egg Cartons",value:(101/1648*100)},
{name:"Plastic Furniture",value:(337/1648*100)},
{name:"Plastic Packing Materials",value:(101/1648*100)},
{name:"Plastic Paint Cans",value:(101/1648*100)},
{name:"Plastic Playsets",value:(337/1648*100)},
{name:"Stainless Steel",value:(505/1648*100)},
{name:"Pool Chemicals",value:(208/1648*100)},
{name:"Porcelain Products",value:(295/1648*100)},
{name:"Power Steering Fluid",value:(292/1648*100)},
{name:"Power Tools",value:(1648/1648*100)},
{name:"Printers",value:(1143/1648*100)},
{name:"Projectors",value:(1143/1648*100)},
{name:"Propane Tanks",value:(713/1648*100)},
{name:"Radiators",value:(589/1648*100)},
{name:"Receivers",value:(1143/1648*100)},
{name:"Record Players",value:(1143/1648*100)},
{name:"Refrigerators",value:(1199/1648*100)},
{name:"Road Flares",value:(292/1648*100)},
{name:"Roofing Materials",value:(295/1648*100)},
{name:"Sand",value:(207/1648*100)},
{name:"Sawdust",value:(295/1648*100)},
{name:"Scanners",value:(1143/1648*100)},
{name:"Scrap Metal",value:(505/1648*100)},
{name:"Sealers",value:(208/1648*100)},
{name:"Shingles",value:(295/1648*100)},
{name:"Shoes",value:(236/1648*100)},
{name:"Shredded Paper",value:(354/1648*100)},
{name:"Silver-oxide Batteries",value:(330/1648*100)},
{name:"Six-pack Rings",value:(101/1648*100)},
{name:"Small Appliances",value:(1143/1648*100)},
{name:"Smoke Detectors",value:(208/1648*100)},
{name:"Soiled Paper",value:(354/1648*100)},
{name:"Solvents",value:(208/1648*100)},
{name:"Sporting Goods",value:(236/1648*100)},
{name:"Steel Cans",value:(708/1648*100)},
{name:"Steel Lids",value:(708/1648*100)},
{name:"String Lights",value:(280/1648*100)},
{name:"Stumps",value:(74/1648*100)},
{name:"Surfboards",value:(236/1648*100)},
{name:"Synthetic Cork",value:(236/1648*100)},
{name:"Tablets",value:(1143/1648*100)},
{name:"Telephones",value:(1523/1648*100)},
{name:"Television Accessories",value:(1143/1648*100)},
{name:"Tennis Balls",value:(236/1648*100)},
{name:"Tile",value:(207/1648*100)},
{name:"Tires",value:(129/1648*100)},
{name:"Toasters",value:(1648/1648*100)},
{name:"Toner Cartridges",value:(1523/1648*100)},
{name:"Tools",value:(712/1648*100)},
{name:"Wood Chips",value:(45/1648*100)},
{name:"Toothbrushes",value:(337/1648*100)},
{name:"Toothpaste Tubes",value:(236/1648*100)},
{name:"Two-Way Radios",value:(1143/1648*100)},
{name:"Toys",value:(280/1648*100)},
{name:"Transmission Fluid",value:(292/1648*100)},
{name:"Trophies",value:(236/1648*100)},
{name:"Truck Tires",value:(84/1648*100)},
{name:"Typewriters",value:(1648/1648*100)},
{name:"Yoga Mats",value:(236/1648*100)},
{name:"Tyvek Envelopes",value:(101/1648*100)},
{name:"Vacuum Cleaners",value:(1143/1648*100)},
{name:"Varnish",value:(217/1648*100)},
{name:"VCRs",value:(1143/1648*100)},
{name:"Vehicles",value:(589/1648*100)},
{name:"Video Game Cartridges",value:(1143/1648*100)},
{name:"Video Game Peripherals",value:(1143/1648*100)},
{name:"Video Tapes",value:(1143/1648*100)},
{name:"Vinyl Records",value:(1143/1648*100)},
{name:"Washer/Dryers",value:(944/1648*100)},
{name:"Water Filters",value:(101/1648*100)},
{name:"Waxed Cardboard",value:(354/1648*100)},
{name:"Weeds",value:(74/1648*100)},
{name:"Window Envelopes",value:(354/1648*100)},
{name:"Windows",value:(295/1648*100)},
{name:"Wood",value:(340/1648*100)},
{name:"Wood Furnishings",value:(207/1648*100)},
{name:"Wood Stains",value:(217/1648*100)},
{name:"Wrapping Paper",value:(398/1648*100)},
{name:"Yard Waste",value:(74/1648*100)},
{name:"Zinc",value:(505/1648*100)},
{name:"Zinc-air Batteries",value:(122/1648*100)},
{name:"Zinc-carbon Batteries",value:(122/1648*100)},
]

const df_shelter_data=
[
{name: 1, value: 102 ,percent: 7.25},
{name: 1.5, value: 10,percent:0.71},
{name: 2, value: 40,percent:2.84},
{name: 2.5, value: 70,percent:4.97},
{name: 3, value: 111,percent:7.89},
{name: 3.5, value: 180,percent:12.80},
{name: 4, value: 206,percent:14.65},
{name: 4.5, value: 213,percent:15.14},
{name: 5, value: 474,percent:33.71},
]

const df_org_data=
[
{name: "Bridge to Self Reliance 2000 refugees San Diego CA", value: 342},
{name: "Adopt-a-Rescuer!", value: 59},
{name: "Hurricane Harvey Relief Fund", value: 35333},
{name: "Help Texas Recover after Hurricane Harvey", value: 66},
{name: "Harvey Flood Relief in Texas", value: 103},
{name: "BakerRipley/ Hurricane Harvey Relief", value: 135},
{name: "Hurricane Harvey Texas Relief Fund", value: 38},
{name: "Disaster Relief-Hurricane Harvey", value: 137},
{name: "Lone Star Disaster Relief Fund", value: 104},
{name: "Hurricane Harvey Disaster Relief", value: 18},
{name: "Hurricane Harvey Psychological Support for Victims", value: 18},
{name: "Disaster Relief - Distribution Resources", value: 5},
{name: "Hurricane Irma Relief Fund", value: 15379},
{name: "Target Hunger Hurricane Harvey Recovery Fund", value: 31},
{name: "Hurricane Irma Children's Relief Fund", value: 46},
{name: "Rescuer Training (2017-2018)", value: 16},
{name: "Help Deliver Donated Product to Those in Need", value: 11},
{name: "Hurricane Irma Psychotrauma Support for Victims", value: 8},
{name: "Assist 1,000 Houston Hurricane Harvey Households", value: 19},
{name: "Concert for 2017 Disaster Relief", value: 237},
{name: "GlobalGiving Disaster Response Fund", value: 1735},
{name: "California Wildfire Relief: Helping Children", value: 297},
{name: "Houston Harvey Recovery Direct Assistance", value: 4},
{name: "Emergency Fuel for Disaster Survivors", value: 27},
{name: "Team Rubicon Global - Disaster Response Fund", value: 16},
{name: "California Fires & Debris Flow: Relief & Recovery", value: 0},
{name: "Ventura County Community Disaster Relief Fund", value: 1},
{name: "California Wildfire Disaster Relief for Immigrants", value: 5},
{name: "Rio Grande Valley Flood Response & Recovery", value: 1},
{name: "California Wildfires", value: 0},
{name: "Help Children Displaced by the Williston Fire", value: 0},
{name: "Hurricane Florence Relief Fund", value: 1459},
{name: "Emergency Response to Hurricane Florence", value: 1},
{name: "Hurricane Florence Disaster Response", value: 2},
{name: "Hurricane Florence: Help Babies in North Carolina", value: 75},
{name: "World Central Kitchen Hurricane Florence Response", value: 19},
{name: "Responding to Hurricane Florence", value: 2},
{name: "Stabilizing Communities after Hurricane Florence", value: 4},
{name: "Hurricane Florence First Response and Flood Relief", value: 3},
{name: "Mercy Corps Responds to Hurricane Florence", value: 2},
{name: "Hurricane Michael Relief Fund", value: 1535},
{name: "Emergency Fuel - Hurricane Michael", value: 5},
{name: "Hurricane Michael Response - Disaster Health Kits", value: 2},
{name: "Hurricane Michael Disaster Response", value: 4},
{name: "Emergency Response to Hurricane Michael", value: 1},
{name: "Hurricane Michael Veteran Relief Team Mission", value: 5},
{name: "Hurricane Michael: Help Babies in Florida", value: 1},
{name: "Food for Hurricane Michael Survivors", value: 3},
{name: "Help Communities Recover After Hurricane Michael", value: 0},
{name: "Habitat for Humanity - Hurricane Michael Response", value: 0}
]

const df_disaster_data=
[
{name: "Fire", value: 307, percent: 13.37},
{name: "Hurricane", value: 1988, percent: 86.63 }
]

class Visual extends Component {

  render() {

    var visual = <div></div>
    var title="";
    switch(this.props.match.params.id){
      case "program":
      visual = <PieChart data={program_visual}/>;
      title="Percent Programs per Program Type"
      break;
      case "item":
      visual = <BarChart data={item_visual} extra="%" height="9000"/>;
      title="% Recyclability of Item"
      break;
      case "facility":
      visual = <PieChart data={facility_visual}/>;
      title="Percent Facilities per Facility Type"
      break;
      case "df_disaster":
      visual = <PieChart data={df_disaster_data}/>;
      title="Percent disasters by type"
      break;
      case "df_shelter":
      visual = <PieChart data={df_shelter_data}/>;
      title="Percent of shelters by rating"
      break;
      case "df_org":
      visual = <BarChart data={df_org_data} height="1300"/>;
      title="Number of donations to organization"
      break;
      default:
      title="";
      visual = <div>Bad URL</div>
      break;
    }
		return (
      <div>
        <div className="title">
          <Button variant="outlined" className="visuallink" component={Link} to="/visual/item" onClick={this.forceUpdate}>Items Visualization</Button>
          <Button variant="outlined" className="visuallink" component={Link} to="/visual/program" onClick={this.forceUpdate}>Programs Visualization</Button>
          <Button variant="outlined" className="visuallink" component={Link} to="/visual/facility" onClick={this.forceUpdate}>Facilities Visualization</Button>
          <Button variant="outlined" className="visuallink" component={Link} to="/visual/df_shelter" onClick={this.forceUpdate}>DF shelter Visualization</Button>
          <Button variant="outlined" className="visuallink" component={Link} to="/visual/df_org" onClick={this.forceUpdate}>DF org. Visualization</Button>
          <Button variant="outlined" className="visuallink" component={Link} to="/visual/df_disaster" onClick={this.forceUpdate}>DF disaster Visualization</Button>
          <Typography variant="h4">{title}</Typography>
        </div>
        <div className="visualwrapper">
          {visual}
        </div>
      </div>

		);
	}
}

export default Visual;
