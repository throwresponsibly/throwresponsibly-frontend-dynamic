import React, { Component } from 'react';
import * as d3 from "d3";


class BarChart extends Component {

  componentDidMount(){
    var data = this.props.data;
    var extra = this.props.extra;
    var textra = "";
    if(extra !== undefined){
      textra = extra;
    }
    var margin = ({top: 30, right: 0, bottom: 10, left: 300});
    var height = data.length * 25 + margin.top + margin.bottom;
    var width = 900;

      var format = d3.format(".0f")
      var x = d3.scaleLinear()
    .domain([0, d3.max(data, d => d.value)])
    .range([margin.left, width - margin.right-100]);
    var y = d3.scaleBand()
    .domain(data.map(d => d.name))
    .range([margin.top, height - margin.bottom])
    .padding(0.1);

    var xAxis = g => g
    .attr("transform", `translate(0,${margin.top})`)
    .call(d3.axisTop(x).ticks(width / 100))
    .call(g => g.select(".domain").remove());

    var yAxis = g => g
    .attr("transform", `translate(${margin.left},0)`)
    .call(d3.axisLeft(y).tickSizeOuter(0));


      var chart = function()
      {
          const svg = d3.select("svg");

          svg.append("g")
              .attr("fill", "steelblue")
            .selectAll("rect")
            .data(data)
            .enter().append("rect")
              .attr("x", x(0))
              .attr("y", d => y(d.name))
              .attr("width", d => x(d.value) - x(0)+10)
              .attr("height", y.bandwidth());

          svg.append("g")
              .attr("fill", "black")
              .attr("text-anchor", "end")
              .style("font", "12px sans-serif")
            .selectAll("text")
            .data(data)
            .enter().append("text")
              .attr("x", d => x(d.value)+50)
              .attr("y", d => y(d.name) + y.bandwidth()/2)
              .attr("dy", "0.35em")
              .text(d => format(d.value) + textra);

          svg.append("g")
              .call(xAxis);

          svg.append("g")
              .call(yAxis);

          return svg.node();
        };
        chart();
      }
  render() {
		return (
      <svg height={this.props.height} width="1300" className="visual"></svg>
		);
	}
}

export default BarChart;
