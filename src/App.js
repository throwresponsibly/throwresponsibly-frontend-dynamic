import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, Switch} from "react-router-dom";
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Paper from '@material-ui/core/Paper';

import Home from './home/Home';
import Facilities from './facilities/Facilities';
import Facility from './facilities/Facility';
import Items from './items/Items';
import Item from './items/Item';
import Programs from './programs/Programs';
import Program from './programs/Program';
import About from './about/About';
import Search from './search/Search';
import Visual from './visualization/Visual';

import './App.css';


class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <div>
            <AppBar position="static" className="App-header">
              <Toolbar>
                <Typography variant="h6" color="inherit" className="App-header-name">ThrowResponsibly</Typography>
                <Button component={Link} to="/" color="inherit">Home</Button>
                <Button component={Link} to="/search" color="inherit">Search</Button>
                <Button component={Link} to="/facilities" color="inherit">Facilities</Button>
                <Button component={Link} to="/items" color="inherit">Items</Button>
                <Button component={Link} to="/programs" color="inherit">Programs</Button>
                <Button component={Link} to="/about" color="inherit">About</Button>
              </Toolbar>
            </AppBar>
            <div className="App-body">
              <Paper className="App-page" elevation={1}>
                <Switch>
                  <Route exact path="/" component={Home} />
                  <Route path="/facilities" component={Facilities} />
                  <Route path="/items" component={Items} />
                  <Route path="/programs" component={Programs} />
                  <Route path="/about" component={About} />
                  <Route path="/search" component={Search} />
                  <Route path="/facility/:id" component={Facility}/>
                  <Route path="/program/:id" component={Program} />
                  <Route path="/item/:id" component={Item}/>
                  <Route path="/visual/:id" component={Visual}/>
                  <Route component={({ location }) => {
                    return (
                      <div style={{textAlign: "center", padding: "2vh 0 4vh 0"}}>
                        <Typography variant="h1" color="inherit" className="App-header-name">404</Typography>
                        <Typography variant="h5" color="inherit" className="App-header-name">No match for {location.pathname}</Typography>
                      </div>
                    );
                  }} />
                </Switch>
              </Paper>
            </div>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
